<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['api/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['api/hello', ['method' => 'post']],
    ],
];
//
//
//return Route::resource('attribute','api/attribute');
//return Route::get('attribute/api','api/attribute');
//Route::any('api/classify/update','api/classify/update'); // 定义put请求路由规则
