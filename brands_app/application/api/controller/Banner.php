<?php

namespace app\api\controller;
use app\common\controller\Base;
use app\common\controller\Token;
use think\Controller;
use think\Db;
use think\Request;
use think\Response;
use think\File;
class Banner extends Base
{
    /**
     * 轮播图管理
     */
    public function index()
    {
        //显示列表  判断请求方法是否符合要求
        if (Request::instance()->isGet()){
            //获取查询数据
            $page = Request::instance()->get('page',null);//查询的页数
            $page = empty($page) ? 1 : $page;//判断是否有分页 如果没有则默认第一页
            $list = Db::name('banner')->field('id,image_url,sort,type,shelves,type,addTime')->order('addTime desc')->select();
            //判断是否有数据
            if (empty($list)){
                $this->responseJson([],0);
            }
            $data = paging($list,$page);
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    public function save(Request $request)
    {
        //添加数据    判断请求方法是否符合要求
        if (Request::instance()->isPost()){
            //接受要保存的数据
            $token    = $request->post('token',null);//token
            $type     = $request->post('type',null);//类型,1、首页轮播图，2、品牌展示轮播图
            $sort     = $request->post('sort',null);//展示顺序
            $img      = $request->post('image',null);//图片
            //判断条件是否符合要求
            if (empty($token)){
                $this->responseJson([],1007);
            }
            if (empty($img)){
                $this->responseJson([],3116);
            }
            if(strpos($img,'base64')){
                $tmp = upload($img);
                $img = json_encode($tmp);
            }
            if (empty($type)){
                $this->responseJson([],3117);
            }
            if (empty($sort)){
                $this->responseJson([],3119);
            }
            //获取要添加的数据
            $data = [
                'type'      => $type,
                'sort'      => $sort,
                'image_url' => $img,
                'addTime'   => time(),
            ];
            //是否添加成功
            $tmp = Db::name('banner')->insert($data);
            if ($tmp){
                $this->responseJson([],0);
            }
            $this->responseJson([],1001);
        }
        $this->responseJson([],1000);
    }


    public function delete()
    {
        //删除    判断请求方法是否符合要求
        if (Request::instance()->isDelete()){
            //获取要删除的id
            $id = Request::instance()->param('id');
            //判断ID是否为空
           if (empty($id)){
               $this->responseJson([],1000);
           }
           //获取图片路径
           $img = Db::name('banner')->where('id ='.$id)->field('image_url')->find();
           if(empty($img)){
               $this->responseJson([],3120);
           }
//           $img =UPLOAD_PATH.json_decode($img['image_url'])[0];
//           //删除文件
//           $file = unlink($img);
//           //判断是否删除成功
//           if (!$file){
//               $this->responseJson([],2009);
//           }
           $tmp = Db::name('banner')->where('id',$id)->delete();
           if ($tmp == false){
                $this->responseJson([],1001);
           }
           $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }

    /*
     * 上架、下架
     */
    public function shelves()
    {

        //判断请求方式是否符合要求
        if (Request::instance()->isPut()){
            //获取要上架，下架的商品id
            $id = Request::instance()->param('id',null);
            //判断ID是否存在
            if (empty($id)){
                $this->responseJson([],1007);
            }
            $type = Request::instance()->param('type',null);
            if (empty($type)){
                $this->responseJson([],1007);
            }
            $shelves =  Request::instance()->param('shelves',null);
            if ($shelves === null){
                $this->responseJson([],1007);
            }
            //判断上架或下架
            if ($shelves == 1){
                //判断当上架时已上架数量是否大于5张
                $date = Db::name('banner')->where('shelves = 1')->where('type',$type)->count();
                if ($date >= 5){
                    $this->responseJson([],2008);
                }
                $tmp = Db::name('banner')->where('id ='.$id)->update(['shelves' => 1]);
            }elseif($shelves == 0){
                $tmp = Db::name('banner')->where('id = '.$id)->update(['shelves' => 0]);
            }
            //判断上架，下架是否成功
            if (!$tmp){
                $this->responseJson([],1001);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }
    /**
     * 获取已上架商品
     */
    public function shelves_list(){
        if(Request::instance()->isGet()){
            $list = Db::name('goods')->where(['is_shelves' => 1])->select();
            $this->responseJson($list,0);
        }
        $this->responseJson([],1000);
    }
}
