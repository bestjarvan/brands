<?php
/**
 * 定时任务
 * User: qingyun
 * Date: 17/10/27
 * Time: 上午9:45
 */
namespace app\api\controller;


use think\Controller;
use think\Db;
use think\Request;

class Timing extends Controller{

    //按月份保存品牌数据
    public function month()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        $types = trim(date('m',time()),0)-1;
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = $types;
            $list[$key]['addTime'] = time();
        }
        $recordData = Db::name('record')->insertAll($list);
        $timingData = Db::name('timing')->insert(['years'=>$years,'types'=>$types,'addTime'=>time()]);
    }

    public function timing(){
        if (Request::instance()->isGet()){
            $years = date('Y',time());
            $types = trim(date('m',time()),0)-1;
            $data = Db::name('timing')->where('years',$years)->where('types',$types)->find();
            if (!$data){
                responseJson([],1001);
            }
            responseJson($data,0);
        }
    }



    //按年份销毁品牌数据
    public function delete()
    {
        
    }

    //第二月保存品牌数据
    public function second()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 2;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第三月保存品牌数据
    public function third()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 3;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第四月保存品牌数据
    public function fourth()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 4;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第五月保存品牌数据
    public function fives()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 5;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第六月保存品牌数据
    public function six()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 6;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第七月保存品牌数据
    public function seven()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 7;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第八月保存品牌数据
    public function eight()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 8;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第九月保存品牌数据
    public function nine()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 9;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第十月保存品牌数据
    public function ten()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 10;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第十一月保存品牌数据
    public function eleven()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 11;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }

    //第十二月保存品牌数据
    public function twelve()
    {
        $list = Db::name('enterprise')->field('name,logo,title,content,type,Popularity,value,Charity,Support,GProfit,Innovation,Happiness')->select();
        $years = date('Y',time());
        foreach ($list as $key => $val){
            $list[$key]['years'] = $years;
            $list[$key]['types'] = 12;
            $list[$key]['addTime'] = time();
        }
        $data = Db::name('record')->insertAll($list);
    }
}
