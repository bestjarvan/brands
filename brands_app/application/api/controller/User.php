<?php

namespace app\api\controller;

use app\common\controller\Base;
use think\Db;
use think\Request;
use app\api\model\Admin as AdminModel;
use app\api\model\Member as MemberModel;
use app\common\controller\Token;

//运营人员管理
class User extends Base
{
    //用户管理展示页面
    public function index()
    {
        //判断传值方式是否正确
        if(Request::instance()->isGet()){
            $page = Request::instance()->param('page',null);//页码
            //判断页码是否存在
            $page = empty($page) ? 1 : $page;
            $key = Request::instance()->param('key',null);//1 账号  2 昵称
            if ($key == 1){
                $val = 'account';
            }elseif ($key == 2){
                $val = 'name';
            }else{
                $val = 'id';
            }
            $keywords = Request::instance()->param('keywords',null);//关键词
            $list = AdminModel::all(function ($query) use ($val,$keywords){
                $query->where($val,'like','%'.$keywords.'%')->order('addTime DESC');
            });
            if(empty($list)){
                $this->responseJson([],1054);
            }
            $data = paging($list,$page);
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }



    //后台新增人员
    public function addSave()
    {
        //判断请求方式是否正确
        if(Request::instance()->isPost()){
            $eid = Request::instance()->param('eid',null);
            $account = Request::instance()->param('account',null);//获取账号
            if (empty($account)){
                responseJson([],5004);
            }
            $name = Request::instance()->param('name',null);//获取昵称
            if (empty($name)){
                responseJson([],5005);
            }
            $type = Request::instance()->param('type',null);//类型 1、超级管理员，2、管理员
            if (empty($type)){
                responseJson([],5006);
            }
            $adminAccount = AdminModel::get(['account'=>$account]);//判断账号是否存在
            if (!empty($adminAccount)){
                $this->responseJson([],1077);
            }
            $passWord = Request::instance()->param('password',null);//密码
            if (empty($passWord)){
                responseJson([],2006);
            }
            $confirmPass = Request::instance()->param('confirmPass',null);//确认密码
            if (empty($confirmPass)){
                responseJson([],2003);
            }
            if ($passWord != $confirmPass){
                responseJson([],2004);
            }
            $roles = $type;
            $salt = salt();
            $passWord = md5($passWord . $salt . '$%$^%$&^%&');
            $admin = new AdminModel();
            if($type == 3){
                if(empty($eid)){
                    responseJson([],2104);
                }
                $admin->eid    = $eid;
            }
            $admin->account  = $account;
            $admin->name     = $name;
            $admin->password = $passWord;
            $admin->type     = $type;
            $admin->salt     = $salt;
            $admin->roles    = $roles;
            $admin->addTime     = time();
            $admin->realPassword = $confirmPass;
            $data = $admin->save();
            if (!$data){
                responseJson([],1055);
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //获取详情
    public function details(){
        if (Request::instance()->isGet()){
            $id = Request::instance()->param('id',null);
            if (empty($id)){
                responseJson([],5007);
            }
            $data = AdminModel::get(['id'=>$id]);
            if (!$data){
                responseJson([],1054);
            }
            responseJson($data,0);
        }
        responseJson([],1000);
    }


    //后台编辑运营人员
    public function edit()
    {
        //判断请求方式是否正确
        if(Request::instance()->isPost()){
            $id = Request::instance()->param('id',null);
            if (empty($id)){
                responseJson([],5007);
            }
            $eid = Request::instance()->param('eid',null);
            $account = Request::instance()->param('account',null);//获取账号
            if (empty($account)){
                responseJson([],5004);
            }
            $name = Request::instance()->param('name',null);//获取姓名
            if (empty($name)){
                responseJson([],5005);
            }
            $type = Request::instance()->param('type',null);//类型 1、超级管理员，2、管理员
            if (empty($type)){
                responseJson([],5006);
            }
            if($type == 3){
                if(empty($eid)){
                    responseJson([],2104);
                }
            }
            //判断账号是否存在
            $where = "account = '$account' and id != '$id'";
            $adminAccount = AdminModel::get(function ($query) use ($where){
                $query->where($where);
            });
            if (!empty($adminAccount)){
                $this->responseJson([],1077);
            }
            $admin = new AdminModel();
            $data = $admin->save([
                'account' => $account,
                'name'    => $name,
                'type'    => $type,
                'eid'     => $eid ? $eid : null,
            ],['id'=>$id]);
            if($data === false){
                $this->responseJson([],1057);
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //运营人员管理列表
    public function roleList()
    {
        //判断请求方式是否正确
        if(Request::instance()->isGet()){
            $page = Request::instance()->param('page',null);//页码
            //判断页码是否存在
            $page = empty($page) ? 1 : $page;
            //获取模糊搜索用户账号和姓名
            $account = Request::instance()->param('account',null);
            $name    = Request::instance()->param('name',null);
            $where = [];
            if(!empty($account)){
                $where['account'] = ['like','%' . $account . '%'];
            }
            if(!empty($name)){
                $where['name'] = ['like','%' . $name . '%'];
            }
            $data = Db::name('admin')->where($where)->order('addTime desc')->paginate(10,'',['page'=>$page]);
            if(empty($data)){
                $this->responseJson([],1054);
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //后台删除运营人员
    public function delete()
    {
        //判断请求方式是否正确
        if(Request::instance()->isDelete()){
            $id = Request::instance()->param('id',null);//获取删除ID
            if(empty($id)){
                $this->responseJson([],1007);
            }
            $data = AdminModel::destroy($id);
            if(!$data){
                $this->responseJson([],1056);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }

    //后台重置密码
    public function reset()
    {
        //判断请求方式是否正确
        if(Request::instance()->isPost()){
            $id = Request::instance()->param('id',null);//获取重置密码用户的ID
            if(empty($id)){
                $this->responseJson([],1007);
            }
            $tmp = AdminModel::get(['id'=>$id]);
            if(!$tmp){
                $this->responseJson([],1054);
            }
            $passWord = Request::instance()->param('password',null);//密码
            if (empty($passWord)){
                responseJson([],2006);
            }
            $confirmPass = Request::instance()->param('confirmPass',null);//确认密码
            if (empty($confirmPass)){
                responseJson([],2003);
            }
            if ($passWord != $confirmPass){
                responseJson([],2004);
            }
            $passWord = md5($passWord . $tmp['salt'] . '$%$^%$&^%&');
            $admin = new AdminModel();
            $data = $admin->save([
                'password'      => $passWord,
                'realPassword'  => $confirmPass
            ],['id'=>$id]);
            if($data === false){
                $this->responseJson([],1057);
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //修改密码
    public function modifyPass()
    {
        //判断请求方法是否符合要求
        if(Request::instance()->isPost()){
            $token = Request::instance()->param('token',null);
            //判断该账号是否已存在
            $adminData = AdminModel::get(['token'=>$token]);
            if(empty($adminData)){
                $this->responseJson([] , 1075);
            }
            //获取旧密码
            $usedPass = Request::instance()->param('usedPass',null);
            if(empty($usedPass)){
                $this->responseJson([] ,2081);
            }
            if ($usedPass != $adminData['realPassword']){
                $this->responseJson([] ,2082);
            }
            //获取新密码
            $newPass = Request::instance() -> param('newPass' , null);
            if(empty($newPass)){
                $this->responseJson([] ,2009);
            }
            //获取确认密码
            $confirmPass = Request::instance() -> param('confirmPass' , null);
            if(empty($confirmPass)){
                $this->responseJson([] ,2010);
            }
            $admin = new AdminModel();
            $upData = $admin->save([
                'password' => md5($confirmPass.$adminData['salt'].'$%$^%$&^%&'),
                'realPassword' => $confirmPass
            ],['token'=>$token]);
            if(!$upData){
                $this->responseJson([] ,1009);
            }
            $this->responseJson([] ,0);
        }
        $this->responseJson([],1000);
    }

//    添加新闻获取可操作的企业
    public function enterprise()
    {
        if(Request::instance()->isGet()){
            $token = Request::instance()->param('token',null);
            //该帐号的信息
            $adminData = AdminModel::get(['token'=>$token]);
            if(empty($adminData)){
                $this->responseJson([] , 1075);
            }
            $enterprise = [];
            if($adminData['type'] == 3){
                $enterprise = Db::name('enterprise')->where('id',$adminData['eid'])->select();
            }else if($adminData['type'] == 1 || $adminData['type'] == 2){
                $enterprise = Db::name('enterprise')->select();
            }
            $this->responseJson($enterprise,0);
        }
        $this->responseJson([],1000);
    }
}

