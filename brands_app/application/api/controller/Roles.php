<?php

namespace app\api\controller;

use app\common\controller\Base;
use app\api\model\Rolepermissions as RoPerModel;
use app\api\model\Roles as RolesModel;
use app\api\model\Permissions as PerModel;
use app\api\model\Admin as AdminModel;
use think\Request;


class Roles extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //判断请求方式是否符合要求
        if (Request::instance()->isGet()) {
            $role = new RolesModel();
            $data = $role->order('create_time desc')->select();
            foreach ($data as $key => $val) {
                $list[$key]['id'] = $val['id'];
                $list[$key]['name'] = $val['title'];
                $tmp = AdminModel::get(['roles' => $val['id']]);
                if (!empty($tmp)) {
                    $list[$key]['is_ss'] = 1;
                } else {
                    $list[$key]['is_ss'] = 0;
                }
            }
            $list = empty($list) ? [] : $list;
            $this->responseJson($list);
        }
        $this->responseJson([],1000);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //判断请求方式是否符合要求
        if (Request::instance()->isPost()){
            //获取角色名
            $name   =   $request->post('name',null);
            $num    = RolesModel::all();
            foreach ($num as $value){
                $sum[] = $value->title;
            }
            if (count($sum) >= 10){
                $this->responseJson([],1056);
            }
            //判断角色名是否符合要求
            if (empty($name) && mb_strlen($name) > 10){
                $this->responseJson([],1055);
            }
            $pid = $request->post('pid');
            //判断该角色名是否存在
            $tmp = RolesModel::get(['title'=>$name]);
            if (!empty($tmp)){
                $this->responseJson([],1017);
            }
            $roles  =   new RolesModel();
            $roles->title  = $name;
            $roles->create_time =   time();
            $roles->save();
            $id = $roles->getLastInsID();
            $data = new RoPerModel();
            $data->role_id = $id;
            $data->permission_id = json_encode($pid);
            $data->save();
            $this->responseJson([]);
        }
        $this->responseJson([],1000);
    }

    /**
     * 显示指定的资源
     *
     * @return \think\Response
     */
    public function read()
    {
        //
       if (Request::instance()->isGet()){
           $rid = Request::instance()->get('rid');
           if (empty($rid)){
               $this->responseJson([],1007);
           }
           //获取当前信息
           //获取角色的详细信息
           $data = RolesModel::get($rid);
           if (empty($data)){
               $this->responseJson([]);
           }
           //获取该角色下的权限
           $list = RoPerModel::get(['role_id' => $rid]);
           if (empty($list)){
               $data['rolepermissions'] = [];
                $this->responseJson($data);
           }
           $pid  = explode(',',json_decode($list->permission_id));
           $rolepermissions = PerModel::all($pid);
           if (empty($rolepermissions)){
               $data['rolepermissions'] = [];
               $this->responseJson($data);
           }
           foreach ($rolepermissions as $key => $val){
               $tmp[] = $val['title'];
           }
           $data['rolepermissions'] = $tmp;
           $this->responseJson($data);
       }
       $this->responseJson([],1000);

    }


    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function update(Request $request)
    {
        //编辑角色
        if (Request::instance()->isPut()){
            $rid = $request->put('rid',null);//角色id
            if (empty($rid)){
                $this->responseJson([],1007);
            }
            $name = $request->put('name',null);//加色名称
            $pid = $request->put('pid',null);//权限id
            $data = RolesModel::get($rid);
            $data->title = $name;
            $data->save();
            $list = RoPerModel::get(['role_id' => $rid]);
            if (empty($list)){
                $list = new RoPerModel();
                $list->role_id = $rid;
                $list->permission_id = json_encode($pid);
                $list->save();
                $this->responseJson([]);
            }
            $list->role_id = $rid;
            $list->permission_id = json_encode($pid);
            $list->save();
            $this->responseJson([]);

        }
        $this->responseJson([],1000);
    }

    /**
     * 删除指定资源
     *
     * @return \think\Response
     */
    public function delete()
    {
        //判断请求方式是否符合要求
        if (Request::instance()->isDelete()){
            //获取要删除的角色id
            $id =   Request::instance()->param('id',null);
            //判断ID是否存在
            if (empty($id)){
                $this->responseJson([],1000);
            }
            $temp = AdminModel::all(['roles' => $id]);
            if (!empty($temp)){
                $this->responseJson([],1015);
            }
            $tmp = RolesModel::get($id);
            if (empty($tmp)){
                $this->responseJson([],1013);
            }
            RolesModel::destroy($id);
            $this->responseJson([]);
        }
        $this->responseJson([],1000);
    }
    //权限-角色  管理
    public function operation()
    {
        //判断请求方式是否符合要求
        if (Request::instance()->isPost()){
            //获取角色id
            $rid    = Request::instance()->post('rid',null);
            //权限id
            $pid    = Request::instance()->post('pid',null);
            if (empty($rid)){
                $this->responseJson([],1000);
            }
            if (empty($pid)){
                $this->responseJson([],1000);
            }
            //判断角色 权限是否存在
            $tmps = RolesModel::get($rid);
            if (empty($tmps)){
                $this->responseJson([],1013);
            }
            $tmp = RoPerModel::get($rid);
            if (!empty($tmp)){
                RoPerModel::update(['permission_id' =>json_encode($pid),'role_id' => $rid,'id'=>$tmp['id']]);
                $this->responseJson([]);
            }elseif(empty($tmp)){
                $list = RoPerModel::create([
                    'role_id' => $rid,
                    'permission_id' => json_encode($pid),
                ]);
                if (empty($list)){
                    $this->responseJson([],1001);
                }
                $this->responseJson([]);
            }
        }
        $this->responseJson([],1000);
    }
    //获取权限
    public function authority()
    {
        //判断请求方式是否符合要求
        if (Request::instance()->isGet()){
            $list = PerModel::all();
            foreach ($list as $key=>$value){
                $data[$key]['id'] = $value['id'];
                $data[$key]['name'] = $value['title'];
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }
    /*
     * 获取角色下指定权限
     */
    public function one_authority()
    {
        //判断请求方式是否符合要求
        if (Request::instance()->isGet()){
            //获取角色id
            $id = Request::instance()->get('id',null);
            if (empty($id)){
                $this->responseJson([],1013);
            }
            //获取该角色下权限
            $tmp = RoPerModel::all(['role_id' => $id]);
            //判断该角色下是否有权限
            if (empty($tmp)){
                $this->responseJson([]);
            }
            foreach ($tmp as $k=>$v){
                $data[$k]['roles'] = $v['role_id'];
                $data[$k]['permission'] = explode(',',json_decode($v['permission_id'],true));
            }
            $this->responseJson($data,0);
        }
    }
}
