<?php

namespace app\api\controller;

use app\common\controller\Base;
use app\api\model\Rolepermissions as RoPerModel;
use app\api\model\Roles as RolesModel;
use app\api\model\Permissions as PerModel;
use app\api\model\Admin as AdminModel;
use think\Db;
use think\Request;


class Enterprise extends Base
{
    /**
     * 企业（品牌管理）
     */

    //企业（品牌）展示
    public function index(){
        if (Request::instance()->isGet()){
            $token = Request::instance()->param('token',null);
            $type  = Request::instance()->param('type',null);//1、超级管理员，2、管理员
            if (empty($type)){
                $this->responseJson([],3018);
            }
            $page = Request::instance()->param('page',null);
            $page = (empty($page)) ? 1 : $page ;
            $key = Request::instance()->param('key',null);//1、企业名称
            if ($key == 1){
                $val = 'name';
            }else{
                $val = 'id';
            }
            $keywords = Request::instance()->param('keywords',null);//关键词

            $where = '1=1';
            $brands = Request::instance()->param('brands',null);//1、是百年 2、是准百年
            if (empty($brands)){
                $this->responseJson([],3041);
            }elseif ($brands == 1){
                $where .= ' and type ='.$brands;
            }elseif ($brands == 2){
                $where .= ' and type ='.$brands;
            }
            $memberData = Db::name('admin')->where('token',$token)->find();
            if ($type == 1){
                $list = Db::name('enterprise')->where($where)->where($val,'like','%'.$keywords.'%')->order('addTime desc')->select();
            }else{
                $list = Db::name('enterprise')->where($where)->where($val,'like','%'.$keywords.'%')->where('pid',$memberData['id'])->order('addTime desc')->select();
            }
            $data = paging($list,$page);
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }


    //后台添加企业（品牌）
    public function add(){
        if (Request::instance()->isPost()){
            $token = Request::instance()->param('token',null);
            $memberData = Db::name('admin')->where('token',$token)->find();
            $name = Request::instance()->param('name',null);//企业名称
            if (empty($name)){
                $this->responseJson([],3019);
            }
            $logo = Request::instance()->param('logo',null);//品牌LOGO图片
            if (empty($logo)){
                $this->responseJson([],3020);
            }
            $title = Request::instance()->param('title',null);//品牌名全称
            if (empty($title)){
                $this->responseJson([],3021);
            }
            $tmp = Db::name('enterprise')->where('title',$title)->find();
            if (!empty($tmp)){
                $this->responseJson([],3042);
            }
            $content = Request::instance()->param('content',null);//内容
//            if (empty($content)){
//                $this->responseJson([],3022);
//            }
            $type = Request::instance()->param('type',null);//归类，1、百年品牌，2、准百年品牌
            if (empty($type)){
                $this->responseJson([],3023);
            }
//            $Popularity = Request::instance()->param('Popularity',null);//网络人气榜（人气值）
//            if (empty($Popularity)){
//                $this->responseJson([],3024);
//            }
            $value = Request::instance()->param('value',null);//品牌价值榜（价值）
//            if (empty($value)){
//                $this->responseJson([],3025);
//            }
            $Charity = Request::instance()->param('Charity',null);//爱心公益榜（公益额）
//            if (empty($Charity)){
//                $this->responseJson([],3026);
//            }
            $Support = Request::instance()->param('Support',null);//商业赞助榜（赞助金）
//            if (empty($Support)){
//                $this->responseJson([],3027);
//            }
            $GProfit = Request::instance()->param('GProfit',null);//企业实力榜（利润）
//            if (empty($GProfit)){
//                $this->responseJson([],3028);
//            }
            $Innovation = Request::instance()->param('Innovation',null);//与时俱进榜（创新力）
//            if (empty($Innovation)){
//                $this->responseJson([],3029);
//            }
            $Happiness = Request::instance()->param('Happiness',null);//幸福指数榜（幸福指数）
//            if (empty($Happiness)){
//                $this->responseJson([],3030);
//            }

            if(strpos($logo,'base64')){
                $logo = upload($logo);
                $logo = json_encode($logo);
            }
            $arr = [
                'pid' => $memberData['id'],
                'name' => $name,
                'logo' => $logo,
                'title' => $title,
                'content' => $content,
                'type' => $type,
                'value' => $value,
                'Charity' => $Charity,
                'Support' => $Support,
                'GProfit' => $GProfit,
                'Innovation' => $Innovation,
                'Happiness' => $Happiness,
                'addTime' => time()
            ];
            $data = Db::name('enterprise')->insert($arr);
            if (!$data){
                $this->responseJson([],1055);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }

    //后台修改企业（品牌）
    public function edit(){
        if (Request::instance()->isPost()){
            $token = Request::instance()->param('token',null);
            $memberData = Db::name('admin')->where('token',$token)->find();
            $id = Request::instance()->param('id',null);//获取id
            if (empty($id)){
                $this->responseJson([],5007);
            }
            $name = Request::instance()->param('name',null);//企业名称
            if (empty($name)){
                $this->responseJson([],3019);
            }
            $logo = Request::instance()->param('logo',null);//品牌LOGO图片
            if (empty($logo)){
                $this->responseJson([],3020);
            }
            $title = Request::instance()->param('title',null);//品牌名全称
            if (empty($title)){
                $this->responseJson([],3021);
            }
            $content = Request::instance()->param('content',null);//内容
//            if (empty($content)){
//                $this->responseJson([],3022);
//            }
            $type = Request::instance()->param('type',null);//归类，1、百年品牌，2、准百年品牌
            if (empty($type)){
                $this->responseJson([],3023);
            }
//            $Popularity = Request::instance()->param('Popularity',null);//网络人气榜（人气值）
//            if (empty($Popularity)){
//                $this->responseJson([],3024);
//            }
            $value = Request::instance()->param('value',null);//品牌价值榜（价值）
//            if (empty($value)){
//                $this->responseJson([],3025);
//            }
            $Charity = Request::instance()->param('Charity',null);//爱心公益榜（公益额）
//            if (empty($Charity)){
//                $this->responseJson([],3026);
//            }
            $Support = Request::instance()->param('Support',null);//商业赞助榜（赞助金）
//            if (empty($Support)){
//                $this->responseJson([],3027);
//            }
            $GProfit = Request::instance()->param('GProfit',null);//企业实力榜（利润）
//            if (empty($GProfit)){
//                $this->responseJson([],3028);
//            }
            $Innovation = Request::instance()->param('Innovation',null);//与时俱进榜（创新力）
//            if (empty($Innovation)){
//                $this->responseJson([],3029);
//            }
            $Happiness = Request::instance()->param('Happiness',null);//幸福指数榜（幸福指数）
//            if (empty($Happiness)){
//                $this->responseJson([],3030);
//            }
//            $home = Request::instance()->param('home',null);//国内外品牌
//            if (empty($home)){
//                $this->responseJson([],3031);
//            }
            if(strpos($logo,'base64')){
                $logo = upload($logo);
                $logo = json_encode($logo);
            }else{
                $logo = $logo;
            }
            $arr = [
                'name' => $name,
                'logo' => $logo,
                'title' => $title,
                'content' => $content,
                'type' => $type,
                'value' => $value,
                'Charity' => $Charity,
                'Support' => $Support,
                'GProfit' => $GProfit,
                'Innovation' => $Innovation,
                'Happiness' => $Happiness,
                'addTime' => time()
            ];
            $data = Db::name('enterprise')->where('id',$id)->update($arr);
            if ($data === false){
                $this->responseJson([],1009);
            }
            $this->responseJson([],0);
        }
        $this->responseJson( [],1000);
    }

    //获取详情
    public function details()
    {
        if (Request::instance()->isGet()){
            $id = Request::instance()->param('id',null);//获取id
            if (empty($id)){
                $this->responseJson([],5007);
            }
            $data = Db::name('enterprise')->where('id',$id)->find();
            if (!$data){
                $this->responseJson([],1054);
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //删除
    public function delete(){
       if (Request::instance()->isDelete()){
           $id = Request::instance()->param('id',null);//获取id
           if (empty($id)){
               $this->responseJson([],5007);
           }
           $data = Db::name('enterprise')->where('id',$id)->delete();
           if ($data === false){
               $this->responseJson([],1056);
           }
           $this->responseJson([],0);
       }
       $this->responseJson([],1000);
    }


    //百年品牌上下架
    public function shelves(){
        if (Request::instance()->isPut()){
            $id = Request::instance()->param('id',null);
            //判断ID是否为空
            if (empty($id)){
                $this->responseJson([],5007);
            }
            $shelves = Request::instance()->param('shelves',null);//是否上架 1、上架 2、下架
            if (empty($shelves)){
                $this->responseJson([],1007);
            }
            $data = Db::name('enterprise')->where('id',$id)->update(['shelves'=>$shelves]);
            if ($data === false){
                $this->responseJson([],1001);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }

}
