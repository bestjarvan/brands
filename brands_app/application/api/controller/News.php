<?php

namespace app\api\controller;

use app\common\controller\Base;
use app\api\model\Rolepermissions as RoPerModel;
use app\api\model\Roles as RolesModel;
use app\api\model\Permissions as PerModel;
use app\api\model\Admin as AdminModel;
use think\Db;
use think\Request;


class News extends Base
{
    /**
     * 企业新闻（品牌管理）
     */

    //企业（品牌）展示
    public function index(){
        if (Request::instance()->isGet()){
            $token = Request::instance()->param('token',null);
            $type  = Request::instance()->param('type',null);//1、超级管理员，2、管理员
            if (empty($type)){
                $this->responseJson([],3018);
            }
            $page = Request::instance()->param('page',null);
            $page = (empty($page)) ? 1 : $page ;
            $keyType = Request::instance()->param('key',null);//1、新闻标题 2、企业名称
            if($keyType == 1){
                $searchType = 'title';
            }else{
                $searchType = 'name';
            }
            $keywords = Request::instance()->param('keywords',null);//关键词
            $memberData = Db::name('admin')->where('token',$token)->find();
            $enterpriseData = Db::name('enterprise');

            if ($type == 1 || $type == 2){
                if ($searchType != 'name'){
                    $list = Db::name('new')->where($searchType,'like','%'.$keywords.'%')->order('releaseTime desc')->select();
                }else{
                    $searchList = [];
                    $list = Db::name('new')->select();
                    $nameList = Db::name('enterprise')->where($searchType,'like','%'.$keywords.'%')->select();
                    foreach ($nameList as $k => $v){
                        foreach ($list as $i => $val){
                            if($v['id'] == $list[$i]['pid']){
                                array_push($searchList,$v['id']);
                            }
                        }
                    }
                    $list = [];
                    foreach ($searchList as $k => $v){
                        $_list = Db::name('new')->where('pid',$v)->order('releaseTime desc')->select();
                        $list = array_merge_recursive($list,$_list);
                    }
                }
            }else{
                if($searchType != 'name'){
                    $list = Db::name('new')->where($searchType,'like','%'.$keywords.'%')->where('aid',$memberData['id'])->order('releaseTime desc')->select();
                }else{
//                    $list = Db::name('new')->where('aid',$memberData['id'])->order('releaseTime desc')->select();
                    $searchList = [];
                    $list = Db::name('new')->where('aid',$memberData['id'])->select();
                    $nameList = Db::name('enterprise')->where($searchType,'like','%'.$keywords.'%')->where('id',$memberData['eid'])->select();
                    foreach ($nameList as $k => $v){
                        foreach ($list as $i => $val){
                            if($v['id'] == $list[$i]['pid']){
                                array_push($searchList,$v['id']);
                            }
                        }
                    }
                    $list = [];
                    foreach ($searchList as $k => $v){
                        $_list = Db::name('new')->where('pid',$v)->order('releaseTime desc')->select();
                        $list = array_merge_recursive($list,$_list);
                    }
                }
            }
            foreach ($list as $key => $val){
                $name = $enterpriseData->where('id',$val['pid'])->find();
                $list[$key]['enterprise'] = $name['name'];
            }
            $data = paging($list,$page);
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }


    //后台添加企业（品牌）新闻
    public function save(){
        if (Request::instance()->isPost()){
            $token = Request::instance()->param('token',null);
            $adminData = Db::name('admin')->where('token',$token)->find();
            $pid = Request::instance()->param('pid',null);//企业品牌ID
            if (empty($pid)){
                $this->responseJson([],3035);
            }
            $title = Request::instance()->param('title',null);//新闻标题
            if (empty($title)){
                $this->responseJson([],3036);
            }
            $cover = Request::instance()->param('cover',null);//新闻封面图
            if (empty($cover)){
                $this->responseJson([],3045);
            }
            $content = Request::instance()->param('content',null);//新闻内容
            if (empty($content)){
                $this->responseJson([],3037);
            }
            $type = Request::instance()->param('type',null);//发布类型，1、品牌快讯，2、公司动态、3、市场活动，4、媒体报道
            if (empty($type)){
                $this->responseJson([],3038);
            }
            $releaseTime = Request::instance()->param('releaseTime',null);//发布时间
            if (empty($releaseTime)){
                $this->responseJson([],3039);
            }
            $cover = json_encode(upload($cover));
            $releaseTime = strtotime($releaseTime);
            $arr = [
                'aid'         => $adminData['id'],
                'pid'         => $pid,
                'title'       => $title,
                'cover'       => $cover,
                'content'     => $content,
                'type'        => $type,
                'releaseTime' => $releaseTime,
                'addTime'     => time()
            ];
            $data = Db::name('new')->insert($arr);
            if (!$data){
                $this->responseJson([],1055);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }

    //后台修改企业（品牌）新闻
    public function edit(){
        if (Request::instance()->isPost()){
            $id = Request::instance()->param('id',null);//获取id
            if (empty($id)){
                $this->responseJson([],5007);
            }
            $token = Request::instance()->param('token',null);
            $adminData = Db::name('admin')->where('token',$token)->find();
            $pid = Request::instance()->param('pid',null);//企业品牌ID
            if (empty($pid)){
                $this->responseJson([],3035);
            }
            $title = Request::instance()->param('title',null);//新闻标题
            if (empty($title)){
                $this->responseJson([],3036);
            }
            $cover = Request::instance()->param('cover',null);//新闻封面图
            if (empty($cover)){
                $this->responseJson([],3045);
            }
            $content = Request::instance()->param('content',null);//新闻内容
            if (empty($content)){
                $this->responseJson([],3037);
            }
            $type = Request::instance()->param('type',null);//发布类型，1、品牌快讯，2、公司动态、3、市场活动，4、媒体报道
            if (empty($type)){
                $this->responseJson([],3038);
            }
            $releaseTime = Request::instance()->param('releaseTime',null);//发布时间
            if (empty($releaseTime)){
                $this->responseJson([],3039);
            }
            $releaseTime = strtotime($releaseTime);
            if(strpos($cover,'base64')){
                $img = upload($cover);
                $img = json_encode($img);
            }else{
                $img = $cover;
            }

            $arr = [
                'aid'         => $adminData['id'],
                'pid'         => $pid,
                'title'       => $title,
                'cover'       => $img,
                'content'     => $content,
                'type'        => $type,
                'releaseTime' => $releaseTime,
            ];
            $data = Db::name('new')->where('id',$id)->update($arr);
            if ($data === false){
                $this->responseJson([],1057);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }



    //获取新闻详情
    public function details()
    {
        if (Request::instance()->isGet()){
            $id = Request::instance()->param('id',null);//获取id
            if (empty($id)){
                $this->responseJson([],5007);
            }
            $data = Db::name('new')->where('id',$id)->find();
            if (!$data){
                $this->responseJson([],1054);
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //删除新闻
    public function delete(){
       if (Request::instance()->isDelete()){
           $token = Request::instance()->param('token',null);
           $adminData = Db::name('admin')->where('token',$token)->find();
           $id = Request::instance()->param('id',null);//获取id
           $type = Request::instance()->param('type',null);//获取权限
           if (empty($id)){
               $this->responseJson([],5007);
           }
           $tmp = Db::name('new')->where('id',$id)->find();
           if($type != 1 && $type != 2){
               if ($tmp['aid'] != $adminData['id']){
                   $this->responseJson([],1000);
               }
           }
           $data = Db::name('new')->where('id',$id)->delete();
           if ($data === false){
               $this->responseJson([],1056);
           }
           $this->responseJson([],0);
       }
       $this->responseJson([],1000);
    }

//    新闻上下架
    public function shelves(){
        if (Request::instance()->isPut()){
            $id = Request::instance()->param('id',null);
            //判断ID是否为空
            if (empty($id)){
                $this->responseJson([],5007);
            }
            $shelves = Request::instance()->param('shelves',null);//是否上架 1、上架 2、下架
            if (empty($shelves)){
                $this->responseJson([],1007);
            }
            $arr = [
                'shelves' => $shelves,
                'releaseTime' => time()
            ];
            $data = Db::name('new')->where('id',$id)->update($arr);
            if ($data === false){
                $this->responseJson([],1001);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }


}
