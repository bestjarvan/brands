<?php

namespace app\api\controller;
use app\common\controller\Base;
use app\common\controller\Token;
use think\Controller;
use think\Db;
use think\Request;
use think\Response;
use think\File;

/**
 * 背景图管理
 */
class Image extends Base
{

    //背景图片列表
    public function index()
    {
        if (Request::instance()->isGet()){
            //获取查询数据
            $page = Request::instance()->get('page',null);//查询的页数
            $page = empty($page) ? 1 : $page;//判断是否有分页 如果没有则默认第一页
            $list = Db::name('image')->select();
            $data = paging($list,$page,10);
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    public function save()
    {
        if (Request::instance()->isPost()){
            $type = Request::instance()->param('type',null);//1，图一，2、图二，3、图三，4、图四
            if (empty($type)) {
                $this->responseJson([],3031);
            }
            $img = Request::instance()->param('img',null);//背景图片
            if (empty($img)) {
                $this->responseJson([],3032);
            }
            if(strpos($img,'base64')){
                $img = upload($img);
                $img = json_encode($img);
            }else{
                $img = $img;
            }
            $arr = [
                'type' => $type,
                'img' => $img,
                'addTime' => time()
            ];
            $data = Db::name('image')->insert($arr);
            if (!$data){
                $this->responseJson([],1055);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }


    public function delete()
    {
        //删除    判断请求方法是否符合要求
        if (Request::instance()->isDelete()){
            //获取要删除的id
            $id = Request::instance()->param('id',null);//获取id
            if (empty($id)){
                $this->responseJson([],5007);
            }
           $tmp = Db::name('image')->where('id',$id)->delete();
           if ($tmp == false){
                $this->responseJson([],1056);
           }
           $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }

    /*
     * 上架、下架
     */
    public function shelves()
    {

        //判断请求方式是否符合要求
        if (Request::instance()->isPut()){
            //获取要上架，下架的商品id
            $id = Request::instance()->param('id',null);
            //判断ID是否存在
            if (empty($id)){
                $this->responseJson([],1000);
            }
            $type = Request::instance()->param('type',null);
            if (empty($type)) {
                $this->responseJson([],3031);
            }
            $shelves =  Request::instance()->param('shelves',null);
            if (empty($shelves)){
                $this->responseJson([],1000);
            }
            //判断上架或下架
            if ($shelves == 1){
                //判断当上架时已上架数量是否大于5张
                $date = Db::name('image')->where('is_shelves',1)->where('type',$type)->count();
                if ($date >= 1){
                    $this->responseJson([],3033);
                }
                $tmp = Db::name('image')->where('id',$id)->update(['is_shelves' => 1]);
            }elseif($shelves == 2){
                $tmp = Db::name('image')->where('id',$id)->update(['is_shelves' => 2]);
            }
            //判断上架，下架是否成功
            if ($tmp === false){
                $this->responseJson([],1001);
            }
            $this->responseJson([],0);
        }
        $this->responseJson([],1000);
    }
    /**
     * 获取已上架商品
     */
    public function shelves_list(){
        if(Request::instance()->isGet()){
            $list = Db::name('goods')->where(['is_shelves' => 1])->select();
            $this->responseJson($list,0);
        }
        $this->responseJson([],1000);
    }
}
