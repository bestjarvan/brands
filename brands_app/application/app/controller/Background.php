<?php
//前台用户注册

namespace app\app\controller;

use app\api\controller\Push;
use app\common\controller\AppBase;
use app\common\controller\AppToken;
use think\exception\HttpResponseException;
use think\Controller;
use think\Db;
use think\Request;

class Background extends  Controller
{

    public function responseJson($data = [],$code = 0)
    {
        $msg =  $GLOBALS['ERR_CODE'][$code] ? $GLOBALS['ERR_CODE'][$code] : '';
        throw new HttpResponseException(json(['code'=>$code,'msg'=> $msg,'data'=>$data]));
    }

    //背景图
    public function index()
    {
        if (Request::instance()->isGet()){
            $type = Request::instance()->param('type',null);//1首页 2品牌
            if(empty($type)){
                $this->responseJson([],1001);
            }
            if($type == 1){
                $data = Db::name('image')->where('type',1 && 2)->where('is_shelves',1)->select();
            }else{
                $data = Db::name('image')->where('type',3 && 4)->where('is_shelves',1)->select();
            }
            $data = empty($data) ? [] : $data;
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }
}
