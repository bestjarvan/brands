<?php

namespace app\app\controller;

use think\Controller;
use think\exception\HttpResponseException;

use think\Db;
use think\Request;

class Home extends Controller
{
    public function responseJson($data = [],$code = 0)
    {
        $msg =  $GLOBALS['ERR_CODE'][$code] ? $GLOBALS['ERR_CODE'][$code] : '';
        throw new HttpResponseException(json(['code'=>$code,'msg'=> $msg,'data'=>$data]));
    }
    /**
     * 显示资源列表
     * 首页
     *
     * @return \think\Response
     */
//    轮播图
    public function banner()
    {
        //判断是否是get请求
        if(Request::instance()->isGet()){
            $type = Request::instance()->param('type',null);
            if(empty($type)){
                $this->responseJson($data = [], $code = 1007);
            }
//            获取上架轮播图 $type == 1 首页 2 品牌
            $banner = Db::name('banner')->where('shelves = 1 and type = '.$type)->field('id,image_url,type')->order('sort asc')->select();
//            判断是否有上架的轮播图
            $banner = empty($banner) ? [] : $banner;
            $this->responseJson($banner, $code = 0);
        }
        $this->responseJson($data = [], $code = 1000);
    }

    //获取首页背景图
    public function image()
    {
        if (Request::instance()->isGet()){
            //获取上架的背景图一、二、三、四
            $data = Db::name('image')->where('is_shelves',1)->select();
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }


    public function index()
    {
        //判断是否是get请求
        if (Request::instance()->isGet()) {
            $page = Request::instance()->param('page',null);
            $page = empty($page) ? 1 : $page ;
            $state = Request::instance()->param('state',null);//1、网路人气榜，2、品牌价值榜，3、爱心公益榜，4、商业赞助榜，5、企业实力榜，6、与时俱进榜，7、幸福指数榜
            $type = Request::instance()->param('type',null);//1、百年品牌，2、准百年品牌

            if (empty($state) || $state == 1){
                //点击网路人气榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('enterprise')->where('type',1)->where('shelves',1)->order('Popularity desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('enterprise')->where('type',2)->where('shelves',1)->order('Popularity desc')->select();
                }
            }elseif($state == 2){
                //点击品牌价值榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('enterprise')->where('type',1)->where('shelves',1)->order('value desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('enterprise')->where('type',2)->where('shelves',1)->order('value desc')->select();
                }
            }elseif($state == 3){
                //点击爱心公益榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('enterprise')->where('type',1)->where('shelves',1)->order('Charity desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('enterprise')->where('type',2)->where('shelves',1)->order('Charity desc')->select();
                }
            }elseif($state == 4){
                //点击商业赞助榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('enterprise')->where('type',1)->where('shelves',1)->order('Support desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('enterprise')->where('type',2)->where('shelves',1)->order('Support desc')->select();
                }
            }elseif($state == 5){
                //点击企业实力榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('enterprise')->where('type',1)->where('shelves',1)->order('GProfit desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('enterprise')->where('type',2)->where('shelves',1)->order('GProfit desc')->select();
                }
            }elseif($state == 6){
                //点击与时俱进榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('enterprise')->where('type',1)->where('shelves',1)->order('Innovation desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('enterprise')->where('type',2)->where('shelves',1)->order('Innovation desc')->select();
                }
            }else{
                //点击幸福指数榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('enterprise')->where('type',1)->where('shelves',1)->order('Happiness desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('enterprise')->where('type',2)->where('shelves',1)->order('Happiness desc')->select();
                }
            }
            $Data = paging($data,$page);
            $this->responseJson($Data,0);
        }
        $this->responseJson($data = [], $code = 1000);
    }

    //年份排行
    public function years()
    {
        //判断是否是get请求
        if (Request::instance()->isGet()) {
            $page = Request::instance()->param('page',null);
            $page = empty($page) ? 1 : $page ;
            $state = Request::instance()->param('state',null);//1、网路人气榜，2、品牌价值榜，3、爱心公益榜，4、商业赞助榜，5、企业实力榜，6、与时俱进榜，7、幸福指数榜
            $type = Request::instance()->param('type',null);//1、百年品牌，2、准百年品牌
            $years = Request::instance()->param('years',null);//年份

            if (empty($state) || $state == 1){
                //点击网路人气榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->order('Popularity desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->order('Popularity desc')->select();
                }
            }elseif($state == 2){
                //点击品牌价值榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->order('value desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->order('value desc')->select();
                }
            }elseif($state == 3){
                //点击爱心公益榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->order('Charity desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->order('Charity desc')->select();
                }
            }elseif($state == 4){
                //点击商业赞助榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->order('Support desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->order('Support desc')->select();
                }
            }elseif($state == 5){
                //点击企业实力榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->order('GProfit desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->order('GProfit desc')->select();
                }
            }elseif($state == 6){
                //点击与时俱进榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->order('Innovation desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->order('Innovation desc')->select();
                }
            }else{
                //点击幸福指数榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->order('Happiness desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->order('Happiness desc')->select();
                }
            }
            $Data = paging($data,$page);
            $this->responseJson($Data,0);
        }
        $this->responseJson($data = [], $code = 1000);
    }


    //月排行
    public function quarterly()
    {
        //判断是否是get请求
        if (Request::instance()->isGet()) {
            $state = Request::instance()->param('state',null);//1、网路人气榜，2、品牌价值榜，3、爱心公益榜，4、商业赞助榜，5、企业实力榜，6、与时俱进榜，7、幸福指数榜
            $type = Request::instance()->param('type',null);//1、百年品牌，2、准百年品牌
            $years = Request::instance()->param('years',null);//年份

            $types = Request::instance()->param('types',null);//1 ~ 12、1 ~ 12月份
            if (empty($state) || $state == 1){
                //点击网路人气榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->where('types',$types)->order('Popularity desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->where('types',$types)->order('Popularity desc')->select();
                }
            }elseif($state == 2){
                //点击品牌价值榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->where('types',$types)->order('value desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->where('types',$types)->order('value desc')->select();
                }
            }elseif($state == 3){
                //点击爱心公益榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->where('types',$types)->order('Charity desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->where('types',$types)->order('Charity desc')->select();
                }
            }elseif($state == 4){
                //点击商业赞助榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->where('types',$types)->order('Support desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->where('types',$types)->order('Support desc')->select();
                }
            }elseif($state == 5){
                //点击企业实力榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->where('types',$types)->order('GProfit desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->where('types',$types)->order('GProfit desc')->select();
                }
            }elseif($state == 6){
                //点击与时俱进榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->where('types',$types)->order('Innovation desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->where('types',$types)->order('Innovation desc')->select();
                }
            }else{
                //点击幸福指数榜
                if (empty($type) || $type == 1){
                    //百年品牌
                    $data = Db::name('record')->where('type',1)->where('years',$years)->where('types',$types)->order('Happiness desc')->select();
                }else{
                    //准百年品牌
                    $data = Db::name('record')->where('type',2)->where('years',$years)->where('types',$types)->order('Happiness desc')->select();
                }
            }
            $this->responseJson($data,0);
        }
        $this->responseJson($data = [], $code = 1000);
    }



}