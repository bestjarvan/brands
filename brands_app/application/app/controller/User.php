<?php
//前台用户注册

namespace app\app\controller;

use app\api\controller\Push;
use app\common\controller\AppBase;
use app\common\controller\AppToken;
use think\exception\HttpResponseException;
use think\Controller;
use think\Db;
use think\Request;

class User extends  Controller
{
    public function responseJson($data = [], $code = 0)
    {
        $msg = $GLOBALS['ERR_CODE'][$code] ? $GLOBALS['ERR_CODE'][$code] : '';
        throw new HttpResponseException(json(['code' => $code, 'msg' => $msg, 'data' => $data]));
    }

    //发送验证码
    public function sendSMS()
    {
        //判断请求方法是否符合要求
        if (Request::instance()->isPost()) {
            //获取用户注册的账号就是手机号
            $account = Request::instance()->param('account', null);
            if (empty($account)) {
                $this->responseJson([], 3000);
            }
            $state = Request::instance()->param('state', null);
            if ($state == 1) {
                $tmp = Db::name('member')->where('account', $account)->find();
                if (!$tmp) {
                    $this->responseJson([], 1075);
                }
            }
            require_once SMS_SDK_PATH;
            $sms = new \SMS();
            $code = $sms->sms_send(['tel' => $account, 'tempId' => 204113]);
            $this->responseJson([], $code);
        }
        $this->responseJson([], 1000);
    }

    //获取验证码并验证
    public function check_sms_code()
    {
        if (Request::instance()->isPost()) {
            $account = Request::instance()->param('account', null);
            $sms_code = Request::instance()->param('sms_code', null);
            if (empty($account) || empty($sms_code)) {
                $this->responseJson([], 1007);
            }
            require_once SMS_SDK_PATH;
            $sms = new \SMS();
            $code = $sms->check_sms_code(['tel' => $account, 'sms_code' => $sms_code]);
            $this->responseJson([], $code);
        }
        $this->responseJson([], 1000);
    }

    /**
     * Token验证
     */
    public function token()
    {
        if (Request::instance()->isGet()) {
            new AppBase();
        }
        $this->responseJson(1000);
    }

    /**
     * 注册验证手机号是否注册过
     */
    public function verification()
    {
        if (Request::instance()->isGet()) {
            $account = Request::instance()->param('account', null);
            if (empty($account)) {
                $this->responseJson([], 1007);
            }
            $tmp = Db::name('member')->where(['account' => $account])->find();
            if (!empty($tmp)) {
                $this->responseJson([], 1010);
            }
            $this->responseJson([], 0);
        }
        $this->responseJson([], 1000);
    }

    /**
     * 保存注册用户的资源
     *
     * @param  \think\Request $request
     * @return \think\Response
     */

    public function register(Request $request)
    {
        //判断请求方法是否符合要求
        if (Request::instance()->isPost()) {
            //获取用户注册的账号
            $account = $request->post('account', null);
            //判断用户注册的账号是否符合要求
            if (empty($account)) {
                $this->responseJson($data = [], $code = 3000);
            }
            //获取用户注册的密码
            $passWord = $request->post('passWord', null);
            //判断用户注册的密码是否符合要求
            if (empty($passWord)) {
                $this->responseJson($data = [], $code = 3001);
            }
            //判断该账号是否已存在
            $data = Db::name('member')->where(['account' => $account])->select();
            if (!empty($data)) {
                $this->responseJson([], 3002);
            }
            $salt = salt();
            //给用户设置的密码加盐并加密
            $passWord = md5($passWord . $salt . '$%$^%$&^%&');
            $userData = [
                'account' => $account,
                'passWord' => $passWord,
                'salt' => $salt
            ];
            //用户信息添加数据库
            $tmp = Db::name('member')->insertGetId($userData);
            //判断是否添加成功
            if (!$tmp) {
                $this->responseJson([], 3003);
            }
            $data = [];
            $tmp = new AppToken();
            $data = $tmp->setToken($account);
            $this->responseJson($data, $code = 0);
        }
        $this->responseJson([], 1000);

    }

    /**
     * 用户登录验证
     * @param Request $request
     */
    public function login(Request $request)
    {
        //判断请求方法是否符合要求
        if (Request::instance()->isPost()) {
            //获取登录用户的账号
            $userName = $request->post('userName', null, 'string');
            if (empty($userName)) {
                $this->responseJson($data = [], $code = 3000);
            }
            //判断该账号是否已存在
            $data = Db::name('member')->where(['account' => $userName])->field('status,passWord,salt,rid')->select();
            if (empty($data)) {
                $this->responseJson($data = [], $code = 2000);
            }
            if ($data[0]['status'] == 1) {
                $this->responseJson([], 2001);
            }
            //获取登录密码
            $passWord = $request->post('passWord', null, 'string');
            if (empty($passWord)) {
                $this->responseJson($data = [], $code = 3001);
            }
            if (md5($passWord . $data[0]['salt'] . '$%$^%$&^%&') != $data[0]['passWord']) {
                $this->responseJson($data = [], $code = 3005);
            }
            $rid = Request::instance()->param('rid', null);//设备id
            if (!empty($rid)) {
                if (!empty($data[0]['rid']) && $rid != $data[0]['rid']) {
                    $push = new Push();
                    $msg = '检测到您的账号在其他设备登录';
                    $pushes = $push->sendMessageOne($msg, $data[0]['rid']);
                }
                Db::name('member')->where(['account' => $userName])->update(['rid' => $rid]);
            }
            $tmp = new AppToken();
            $data = $tmp->setToken($userName);
            $this->responseJson($data, $code = 0);
        }
        $this->responseJson($data = [], $code = 1000);
    }

    //忘记密码
    public function forgetPass()
    {
        //判断请求方法是否符合要求
        if (Request::instance()->isPost()) {
            //获取登录用户的账号
            $userName = Request::instance()->param('userName', null, 'string');
            if (empty($userName)) {
                $this->responseJson($data = [], $code = 3000);
            }
            //判断该账号是否已存在
            $data = Db::name('member')->where(['account' => $userName])->field('passWord,salt')->select();
            if (empty($data)) {
                $this->responseJson($data = [], $code = 2000);
            }
            //获取新密码
            $newPass = Request::instance()->param('newPass', null, 'string');
            if (empty($newPass)) {
                $this->responseJson($data = [], $code = 3001);
            }
            //获取确认密码
            $confirmPass = Request::instance()->param('confirmPass', null, 'string');
            if (empty($confirmPass)) {
                $this->responseJson($data = [], $code = 3010);
            }
            $arr = [
                'passWord' => md5($confirmPass . $data[0]['salt'] . '$%$^%$&^%&'),
            ];
            $updata = Db::name('member')->where(['account' => $userName])->update($arr);
            if (empty($updata)) {
                $this->responseJson($data = [], $code = 3009);
            }
            $tmp = new AppToken();
            $data = $tmp->setToken($userName);
            $this->responseJson($data, $code = 0);
        }
        $this->responseJson($data = [], $code = 1000);
    }

    /**
     * 修改密码
     *
     * @return \think\Response
     */
    public function modifyPass()
    {
        //判断是否是POST提交
        if (Request::instance()->isPost()) {
            //获取登录用户token
            $token = Request::instance()->param('token', null);
            if (!empty($token)) {
                $tmp = Db::name('member')->where(['token' => $token])->field('passWord,salt')->find();
                //获取旧密码
                $passWord = Request::instance()->param('passWord', null);
                if (md5($passWord . $tmp['salt'] . '$%$^%$&^%&') != $tmp['passWord']) {
                    $this->responseJson($data = [], $code = 3005);
                }
                //获取新密码
                $newPass = Request::instance()->param('newPass', null);
                //获取确认密码
                $confirmPass = Request::instance()->param('confirmPass', null);
                if ($newPass != $confirmPass) {
                    $this->responseJson($data = [], $code = 3010);
                }
                $arr = [
                    'passWord' => md5($confirmPass . $tmp['salt'] . '$%$^%$&^%&'),
                ];
                $updata = Db::name('member')->where(['token' => $token])->update($arr);
                if (!empty($updata)) {
                    $this->responseJson($data = [], $code = 0);
                }
                $this->responseJson($data = [], $code = 3009);
            }
        }
        $this->responseJson($data = [], $code = 1000);
    }


    //系统消息
    public function news()
    {
        //获取当前用户token
        $token = Request::instance()->param('token', null);
        if (empty($token)) {
            $this->responseJson([], 1007);
        }
        //判断是否是get传值
        if (Request::instance()->isGet()) {

            //获取当前评价页码
            $page = Request::instance()->param('page', null, 'int');
            //判断当前评价页码为几
            $page = empty($page) ? 1 : $page;
            //获取该用户前端用户消息
            $data = Db::name('member')->alias('m')->join('news n', 'm.account = n.account', 'LEFT')->field('n.title,n.content,n.addTime')->where(['m.token' => $token])->page($page, '10')->select();
            if (empty($data)) {
                $this->responseJson($data = [], $code = 1001);
            }
            $this->responseJson($data, $code = 0);
        }
        $this->responseJson($data = [], $code = 1000);
    }


    //上传头像
    public function image()
    {
        //判断请求方法是否符合要求
        if (Request::instance()->isPut()) {
            $token = Request::instance()->put('token', null);//获取用户token
            $img = Request::instance()->put('img', null);//获取上传头像
            if (empty($token) || empty($img)) {
                $this->responseJson([], 1007);
            }
            $image = upload($img);
            $data = Db::name('member')->where(['token' => $token])->update(['img' => json_encode($image)]);
            if (!$data) {
                $this->responseJson([], 1057);
            }
            $this->responseJson($data, 0);
        }
        $this->responseJson([], 1000);
    }
}
