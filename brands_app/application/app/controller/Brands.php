<?php
//前台用户注册

namespace app\app\controller;

use app\api\controller\Push;
use app\common\controller\AppBase;
use app\common\controller\AppToken;
use think\exception\HttpResponseException;
use think\Controller;
use think\Db;
use think\Request;

class Brands extends  Controller
{

    public function responseJson($data = [],$code = 0)
    {
        $msg =  $GLOBALS['ERR_CODE'][$code] ? $GLOBALS['ERR_CODE'][$code] : '';
        throw new HttpResponseException(json(['code'=>$code,'msg'=> $msg,'data'=>$data]));
    }

    //品牌列表
    public function index()
    {
        if (Request::instance()->isGet()){
            $page = Request::instance()->param('page',null);
            $page = empty($page) ? 1 : $page ;
            $type = Request::instance()->param('type',null);//1百年品牌 2准百年品牌
            $data = Db::name('enterprise')->where('type',$type)->where('shelves',1)->select();
            $data = empty($data) ? [] : $data;
            $Data = paging($data,$page,12);
            $this->responseJson($Data,0);
        }
        $this->responseJson([],1000);
    }

    //品牌详情
    public function detail()
    {
        if(Request::instance()->isGet()){
            $id = Request::instance()->param('id',null);
            $data = Db::name('enterprise')->where('id',$id)->find();
            if(empty($data)){
                $this->responseJson([],3035);
            }
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }
    
}
