<?php
//前台用户注册

namespace app\app\controller;

use app\api\controller\Push;
use app\common\controller\AppBase;
use app\common\controller\AppToken;
use think\exception\HttpResponseException;
use think\Controller;
use think\Db;
use think\Request;

class News extends  Controller
{

    public function responseJson($data = [],$code = 0)
    {
        $msg =  $GLOBALS['ERR_CODE'][$code] ? $GLOBALS['ERR_CODE'][$code] : '';
        throw new HttpResponseException(json(['code'=>$code,'msg'=> $msg,'data'=>$data]));
    }

    //新闻列表
    public function index()
    {
        if (Request::instance()->isGet()){
            $page = Request::instance()->param('page',null);//页
            $page = empty($page) ? 1 : $page ;
            $type = Request::instance()->param('type',null);//类型 1首页 2品牌快讯 3公司动态 4市场活动 5媒体报道
            $type = empty($type) ?  1 : $type;
            if($type != 1){
                $list = Db::name('new')->where('shelves',1)->where('type',$type-1)->order('releaseTime desc')->select();
            }else{
                $list = Db::name('new')->where('shelves',1)->order('releaseTime desc')->select();
            }
            $data = paging($list,$page);
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //新闻排行榜 浏览次数
    public function newRanking()
    {
        if (Request::instance()->isGet()){
            $data = Db::name('new')->where('shelves',1)->order('browse desc')->limit(0,10)->select();
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

    //查看新闻
    public function details()
    {
        if (Request::instance()->isGet()){
            $id = Request::instance()->param('id',null);//新闻id
            $tmp = Db::name('new')->where('id',$id)->setInc('browse',1);//新闻浏览次数加一
            $data = Db::name('new')->where('id',$id)->find();
            $data = empty($data) ? [] : $data;
            $this->responseJson($data,0);
        }
        $this->responseJson([],1000);
    }

}
