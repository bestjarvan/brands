<?php
class JSSDK {
  private $appId;
  private $appSecret;

  public function __construct($appId, $appSecret) {
      $this->appId = $appId;
      $this->appSecret = $appSecret;
  }

  public function getSignPackage($url = null) {
    $jsapiTicket = $this->getJsApiTicket();

    // 注意 URL 一定要动态获取，不能 hardcode.
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    if (empty($url)){
        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
    $timestamp = time();
    $nonceStr = $this->createNonceStr();

    // 这里参数的顺序要按照 key 值 ASCII 码升序排序
    $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

    $signature = sha1($string);

    $signPackage = array(
      "appId"     => $this->appId,
      "nonceStr"  => $nonceStr,
      "timestamp" => $timestamp,
      "url"       => $url,
      "signature" => $signature,
//      "rawString" => $string
    );
    return $signPackage; 
  }

  private function createNonceStr($length = 16) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
  }

  private function getJsApiTicket() {
      // jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
    $data = json_decode($this->get_php_file(__DIR__ . "/jsapi_ticket.php"));
    if ($data->expire_time < time()) {
      $accessToken = $this->getAccessToken();
      // 如果是企业号用以下 URL 获取 ticket
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
      $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
      $res = json_decode($this->httpGet($url));
      $ticket = $res->ticket;
      if ($ticket) {
        $data->expire_time = time() + 7000;
        $data->jsapi_ticket = $ticket;
        $this->set_php_file(__DIR__ . "/jsapi_ticket.php", json_encode($data));
      }
    } else {
      $ticket = $data->jsapi_ticket;
    }

    return $ticket;
  }

  private function getAccessToken() {
      // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
    $data = json_decode($this->get_php_file(__DIR__ . "/access_token.php"));
    if ($data->expire_time < time()) {
      // 如果是企业号用以下URL获取access_token
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
      $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
      $res = json_decode($this->httpGet($url));
      $access_token = $res->access_token;
      if ($access_token) {
        $data->expire_time = time() + 7000;
        $data->access_token = $access_token;
        $this->set_php_file(__DIR__ . "/access_token.php", json_encode($data));
      }
    } else {
      $access_token = $data->access_token;
    }
    return $access_token;
  }

  private function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    // 为保证第三方服务器与微信服务器之间数据传输的安全性，所有微信接口采用https方式调用，必须使用下面2行代码打开ssl安全校验。
    // 如果在部署过程中代码在此处验证失败，请到 http://curl.haxx.se/ca/cacert.pem 下载新的证书判别文件。
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);

    $res = curl_exec($curl);
    curl_close($curl);

    return $res;
  }

  private function get_php_file($filename) {
    return trim(substr(file_get_contents($filename), 15));
  }
  private function set_php_file($filename, $content) {
    $fp = fopen($filename, "w");
    fwrite($fp, "<?php exit();?>" . $content);
    fclose($fp);
  }
    /* 下载媒体素材接口
       * @param {String} 		url
       *  "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token="  图片素材下载
       *  "http://api.weixin.qq.com/cgi-bin/media/get?access_token="       视频/音频临时素材下载
       * @param {String} 		$type
       * @param {String} 		mediaId             下载媒体素材id
       * @param {String} 		savePath            存储媒体素材的文件名,包括绝对路径
       * @returns｛bool｝          result              true 成功，false 失败
      */

    function downloadMedia($mediaIdData){
        $access_token = $this->getAccessToken();
        $url="http://api.weixin.qq.com/cgi-bin/media/get?access_token=";
        $data = [];
        if (is_array($mediaIdData)){
            $ch = curl_init();
            foreach ($mediaIdData as $key => $val){
                $surl =  $url.$access_token . "&media_id=" . $mediaIdData[$key];
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true) ; // 获取数据返回
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, true) ; // 在启用 CURLOPT_RETURNTRANSFER 时候将获取数据返回
                curl_setopt($ch, CURLOPT_URL, $surl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $response = curl_exec($ch);
                $result = false;
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE) ;
                // 获取表单上传文件;
                $tmp =  file_exists(UPLOAD_PATH.'uploads');
                //判断uploads文件夹是否存在
                if (!$tmp)
                    mkdir(UPLOAD_PATH.'uploads');
                //判断文件夹是否存在
                if (!file_exists(UPLOAD_PATH.'uploads'.DS.date('Ymd')))
                    mkdir(UPLOAD_PATH.'uploads'.DS.date('Ymd'));
                //文件写入
                $num =   rand(1000,9999);
                $filePath = DS.date('Ymd').DS.time();
                $savePath = UPLOAD_PATH . 'uploads'. $filePath . $num .'.png';
                if ($status == '200') {
                    if('' != $response){
                        if(file_exists($savePath)){
                            unlink($savePath);
                        }
                        $file = fopen($savePath, "a");
                        fwrite($file, $response);
                        fclose($file);
                        $result = true;
                    }
                }
                if ($result){
                    $data[] = 'uploads'. $filePath . $num .'.png';
                }
            }
            curl_close($ch);
        }else{
            $ch = curl_init();
            $surl =  $url.$access_token . "&media_id=" . $mediaIdData;
            curl_setopt($ch, CURLOPT_URL, $surl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $response = curl_exec($ch);
            $result = false;
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE) ;
            // 获取表单上传文件;
            $tmp =  file_exists(UPLOAD_PATH.'uploads');
            //判断uploads文件夹是否存在
            if (!$tmp)
                mkdir(UPLOAD_PATH.'uploads');
            //判断文件夹是否存在
            if (!file_exists(UPLOAD_PATH.'uploads'.DS.date('Ymd')))
                mkdir(UPLOAD_PATH.'uploads'.DS.date('Ymd'));
            //文件写入
            $num =   rand(1000,9999);
            $filePath = DS.date('Ymd').DS.time();
            $savePath = UPLOAD_PATH . 'uploads'. $filePath . $num .'.png';
            if ($status == '200') {
                if('' != $response){
                    if(file_exists($savePath)){
                        unlink($savePath);
                    }
                    $file = fopen($savePath, "a");
                    fwrite($file, $response);
                    fclose($file);
                    $result = true;
                }
            }
            if ($result){
                $data = 'uploads'. $filePath . $num .'.png';
            }
            curl_close($ch);
        }
        return $data;
    }

}

