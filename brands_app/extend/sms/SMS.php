<?php
use think\Db;
// 定义使用的短信平台("ronglianyun", "luosimao","chuanglan")
define('SMS_PLAT', "ronglianyun");
define('SMS_CONTENT1','【阁德美】您的验证码为');
define('SMS_CONTENT2','，请于1分钟内正确输入，如非本人操作，请忽略此短信。');
class SMS
{
      public function __construct() {
      
      }

        /*
     功能描述:获取短信验证码
     入参:
        account         手机号
        length	　	验证码长度.不传默认为4位
        deadminutes     验证码有效时长默认30分钟
        repeatminutes   允许重发时间默认1分钟
    出参:
        code            验证码
        deadminutes     剩余的过期时间
        leftsecond      禁止重发，查出剩余时间
        status          0.成功，1002.禁止重发，查出剩余时间，1007.参数错误 ,1021.发送短信失败
     */
    function sms_send($body_arr) {
        //判断账号的类型
        $body['tel']        = $body_arr['tel'];
        $body['tempContent'] = array_key_exists('tempContent',$body_arr) ? $body_arr['tempContent'] : null;
        //短信过期时间
        $body['sms_time']   = array_key_exists('sms_time',$body_arr) ? $body_arr['sms_time'] : 5;
        // 允许重复发送时间
        $body['repeat_time']= array_key_exists('repeat_time',$body_arr) ? $body_arr['repeat_time'] : 1;
        $body['tempId']     = array_key_exists('tempId',$body_arr) ? $body_arr['tempId'] : 1;
        //测试id默认为1
        if(!preg_match("/^1[34578]\d{9}$/", $body['tel'])){
            return 1007;
        }
        // 删除掉所有过期的验证码
        $smsData = Db::name('sms')->select();
        $idArr = [];
        foreach ($smsData as $value){
            if ((time() - $value['addTime']) > ($body['sms_time'] * 60)){
                $idArr[] = $value['id'];
            }
        }
        if (count($idArr) > 0){
            Db::name('sms')->delete($idArr);
        }
        $smsArr = Db::name('sms')->where('tel = ' . $body['tel'])->select();

        foreach ($smsArr as $val){
            if ((time() - $val['addTime']) < ($body['repeat_time'] * 60)){
                return 1022;
            }
        }
        require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . SMS_PLAT . DIRECTORY_SEPARATOR . "smsclass.php");

        // 读取平台配置参数
        $sms_config = include(dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.php");
        // 短信平台配置参数
        $param = $sms_config[SMS_PLAT];

        // 接收短信手机号
        $param['phone'] = $body["tel"];

        // 短信内容参数-容联云
        $param['tempId'] = $body["tempId"];
        if (empty($body['tempContent'])){
            $sms_code = rand(1000,9999);
        }else{
            $sms_code = $body['tempContent'];
        }
        $param['datas'] = array($sms_code);
        // 短信内容参数-螺丝帽,容联云易模板中陪的内容
        $param['content'] = '';
        $smsObject = new \SMSClass($param);
        if($smsObject->send()){
            if (empty($body['tempContent'])){
                $smsArray = [
                    'sms_code' => $sms_code,
                    'tel' => $body["tel"],
                    'addTime' => time()
                ];
                Db::name('sms')->insert($smsArray);
            }
            return 0;
        }else{
            return 1021;
        }

    }

 /*
 功能描述: 校验用户输入验证码的合法性
  入参: account       注册账号:手机号码或邮箱
        code          验证码
        type          1:邮箱2:手机
  出参:
        vo_res        0.成功,1003.验证码不对,1007.参数不对,9999.数据库异常
  */
    function check_sms_code($body_arr) {
        if (empty($body_arr['sms_code']) || empty($body_arr['tel'])){
            return 1007;
        }
        $sms_time = array_key_exists('sms_time',$body_arr) ? $body_arr['sms_time'] : 5;
        $smsData = Db::name('sms')->where('tel = ' . $body_arr['tel'] . ' && sms_code = ' . $body_arr['sms_code'])->find();
        if (empty($smsData) || (time() - $smsData['addTime']) > ($sms_time * 60)){
            return 1003;
        }else{
            return 0;
        }
    }
}