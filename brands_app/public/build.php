<?php
/**
 * Created by PhpStorm.
 * User: qingyun
 * Date: 2017/5/11
 * Time: 下午2:27
 */
return [
    // 生成运行时目录
    '__file__' => ['common.php'],
    // 定义index模块的自动生成
    'api'    => [
        '__file__'   => ['common.php'],
        '__dir__'    => ['behavior', 'controller', 'model', 'view'],
        'controller' => ['Index', 'Test', 'UserType'],
        'model'      => [],
        'view'       => ['api/api'],
    ],
    // 。。。 其他更多的模块定义
];