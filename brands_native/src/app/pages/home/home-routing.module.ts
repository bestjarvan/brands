/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeListComponent} from "./home-list/home-list.component";
import {HomeRankComponent} from "./home-rank/home-rank.component";

// 子component

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: HomeListComponent,
            },
            {
                path: 'index/rank',
                component: HomeRankComponent,
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule {}
/**
 * Created by qingyun on 2017/11/20.
 */
