import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {HomeService} from "./home.service";
import {HomeListComponent} from "./home-list/home-list.component";
import {HomeRoutingModule} from "./home-routing.module";
import { HomeRankComponent } from './home-rank/home-rank.component';



@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    PaginationModule,
    ModalModule.forRoot(),
  ],
  providers: [HomeService],
  declarations: [HomeListComponent, HomeRankComponent]
})
export class HomeModule { }
