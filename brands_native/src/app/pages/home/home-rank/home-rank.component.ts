import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HomeService} from "../home.service";
import {LoadingService} from "../../../share/loading.service";

@Component({
  selector: 'app-home-rank',
  templateUrl: './home-rank.component.html',
  styleUrls: ['./home-rank.component.scss']
})
export class HomeRankComponent implements OnInit {
  public title:string;
  public brandsLists:Array<any> = [];
  public select:number = 1;
  public noMore: boolean = false;
  //type: 1、百年 2、准百年
  public params:object = {};

  constructor(public router: ActivatedRoute,
              public service: HomeService,
              public loding: LoadingService
  ) { }

  ngOnInit() {
    let date = new Date();
    this.router.params.subscribe((event) => {
      if(event.types){
        this.title = '网络人气榜 ' + date.getFullYear() + '-' + new Date().getMonth() + ' 月榜';
        this.params = {
          state: event.state,
          type: event.type,
          types: event.types,
          years: event.years
        };
        this.tabClick(event.type);
      }else {
        this.title = '网络人气榜 ' + date.getFullYear() + ' 年度榜';
        this.params = {
          state: event.state,
          type: event.type,
          years: event.years
        };
        this.tabClick(event.type);
      }
    });
  }
  tabClick(index){
    this.params['type'] = index;
    this.select = index;
    if(this.params['types']){
      this.getEnterpriseMonth();
    }else {
      this.getEnterpriseYears();
    }
  }
  //获取月榜单
  getEnterpriseMonth(){
    this.loding.show();
    this.service.getEnterpriseRank(this.params).subscribe(
      res => {
        this.loding.hide();
        if(res['code'] == 0){
          this.brandsLists = res['data'];
          this.brandsLists.map((v) => {
            v['logo'] = IMG_URL + JSON.parse(v['logo'])[0];
          })
        }
      }
    );
  }
  //获取年榜单
  getEnterpriseYears(){
    this.loding.show();
    this.service.getEnterpriseYearsRank(this.params).subscribe(
      res => {
        this.loding.hide();
        if(res['code'] == 0){
          this.brandsLists = res['data']['data'];
          this.brandsLists.map((v) => {
            v['logo'] = IMG_URL + JSON.parse(v['logo'])[0];
          })
        }
      }
    )
  }

}
