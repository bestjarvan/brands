import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConnectListComponent } from './connect-list/connect-list.component';
import {ConnectRoutingModule} from "./connect-routing.module";
import {FormsModule} from "@angular/forms";
import {ModalModule, PaginationModule} from "ngx-bootstrap";
import {ConnectService} from "./connect.service";

import { BaiduMapModule } from 'angular2-baidu-map';


@NgModule({
  imports: [
    CommonModule,
    ConnectRoutingModule,
    FormsModule,
    PaginationModule,
    ModalModule.forRoot(),
    BaiduMapModule.forRoot({ak: '8azVgQbZR9irKHBOsqMzi8CAT7l1gtjt'})
  ],
  providers: [ConnectService],
  declarations: [ConnectListComponent]
})
export class ConnectModule { }
