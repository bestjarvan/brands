/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConnectListComponent} from "./connect-list/connect-list.component";

// 子component

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: ConnectListComponent,
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConnectRoutingModule {}
/**
 * Created by qingyun on 2017/11/20.
 */
