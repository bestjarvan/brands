import { Component, OnInit } from '@angular/core';
import {MapOptions, MarkerOptions, Point} from 'angular2-baidu-map';

@Component({
  selector: 'app-connect-list',
  templateUrl: './connect-list.component.html',
  styleUrls: ['./connect-list.component.scss'],
})

export class ConnectListComponent implements OnInit {
  public options: MapOptions;
  public markers: Array<{ point: Point; options?: MarkerOptions }>;

  constructor() {
    this.options = {
      centerAndZoom: {
        lat: 24.2932994899,
        lng: 116.1301084873,
        zoom: 18,
      },
      enableKeyboard: true,
      enableAutoResize:true,
    };
    // 这是地图标记marker
    this.markers = [{
      options: {
          // icon: {
            // imageUrl: './assets/img/add32.svg',
            // size: {
            //   height: 60,
            //   width: 50
            // }
          // }
        },
        point: {
          lng: 116.1300964873,   // 经度
          lat: 24.2933894899    // 纬度
        }
      }
    ];
  }

  ngOnInit() {

  }
}
