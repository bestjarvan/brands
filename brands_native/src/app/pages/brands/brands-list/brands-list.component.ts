import { Component, OnInit } from '@angular/core';
import {BrandsService} from "../brands.service";
import {LoadingService} from "../../../share/loading.service";

@Component({
  selector: 'app-brands',
  templateUrl: './brands-list.component.html',
  styleUrls: ['./brands-list.component.scss']
})
export class BrandsListComponent implements OnInit {
  public brandsBanner:Array<any> = [];
  public imgUrl:string = IMG_URL;
  public backIamge:Array<any> = [];


  public showTitle:Array<any> = ['百年品牌',"准百年品牌"];
  public select:number = 0;
  public showDiv:boolean = true;

  public showLogo:Array<any> = [];
  public showLogoTwo:Array<any> = [];
  public no_more:boolean = false;
  public scrollData:boolean = false;
  public noMore:boolean = false;
  public params:object = {
    page:1,
    type:1
  };


  constructor(public service:BrandsService,
              public loading:LoadingService,
              ) { }

  ngOnInit(): void {
    this.params['page'] = 1;
    this.getBannerBrands();
    this.getBrandsRanks();
    if(document.body.clientWidth <= 600){
      this.getHomeImg(4);
    }else {
      this.getHomeImg(3);
    }
    let swiper = new Swiper('.swiper-container',{
      //自动轮播
      autoplay:5000,
      //分页
      pagination : '.swiper-pagination',
      //滑动方向  水平滑动
      direction : 'horizontal',
      paginationClickable: true,
      longSwipesRatio: 0.3,
      touchRatio:1,
      //修改swiper自己或子元素时，自动初始化swiper
      observer:true,
      //修改swiper的父元素时，自动初始化swiper
      observeParents:true,
    });
    window.onscroll = () => {
      let height = document.documentElement;
      let scrollTopPx = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
      if(height.scrollHeight - scrollTopPx - height.clientHeight <= height.scrollHeight * .2 && this.scrollData){
        this.scrollData = false;
        this.params['page']++;
        this.upLoadData();
      }
    };

  }
  titleClick(index){
    this.select = index;
    this.params['type'] = index + 1;
    this.params['page'] = 1;
    this.getBrandsRanks();
  };

  getBannerBrands(){
      const params = {
        type:2
      };
      this.service.getBanner(params).subscribe(
        res => {
          if(res['code'] == 0){
            this.brandsBanner = res['data'];
            this.brandsBanner.map((v) => {
              v['image_url'] = JSON.parse(v['image_url'])[0];
            });
          }
        }
      );
  }
  getBrandsRanks(){
    this.loading.show();
    this.service.getBannerRank(this.params).subscribe(
      res => {
        this.loading.hide();
        if(res['code'] == 0){
          this.showLogo = res['data']['data'];
          if(res['data'] && res['data']['data'].length >= 12){
            this.scrollData = true;
          }else {
            this.scrollData = false;
          }
          if(this.showLogo.length == 0){
            this.no_more = true;
          }else {
            this.no_more = false;
          }
          this.showLogo.map((v) => {
            v['logo'] = JSON.parse(v['logo'])[0];
          });
        }
      }
    );
  }
  upLoadData(){
    this.service.getBannerRank(this.params).subscribe(
      res => {
        this.loading.hide();
        if(res['code'] == 0){
          if(res['data'] && res['data']['data'].length >= 12){
            this.scrollData = true;
          }else {
            this.noMore = true;
            this.scrollData = false;
          }
          if(res['data']['data'].length == 0){
            this.no_more = true;
          }else {
            this.no_more = false;
          }
          res['data']['data'].map((v) => {
            v['logo'] = JSON.parse(v['logo'])[0];
          });
          this.showLogo = this.showLogo.concat(res['data']['data']);


        }
      }
    );
  }
  //获取背景图
  getHomeImg(tmp){
    const params = {
      type:tmp
    };
    this.service.getBackImage(params).subscribe(
      res =>{
        if(res['code'] == 0){
          this.backIamge = res['data'];
          if(this.backIamge){
            this.backIamge.map((v) => {
              v['img'] = IMG_URL + JSON.parse(v['img'])[0];
            })
          }
        }
      }
    )
  }

}
