import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {BrandsRoutingModule} from "./brands-routing.module";
import {BrandsService} from "./brands.service";
import {BrandsListComponent} from "./brands-list/brands-list.component";
import { BrandsDetailComponent } from './brands-detail/brands-detail.component';


@NgModule({
  imports: [
    CommonModule,
    BrandsRoutingModule,
    FormsModule,
    PaginationModule,
    ModalModule.forRoot(),
  ],
  providers: [BrandsService],
  declarations: [BrandsListComponent, BrandsDetailComponent]
})
export class BrandsModule { }
