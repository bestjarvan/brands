import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BrandsService} from "../brands.service";
import {NgxEchartsService} from 'ngx-echarts';

@Component({
  selector: 'app-brands-detail',
  templateUrl: './brands-detail.component.html',
  styleUrls: ['./brands-detail.component.scss']
})
export class BrandsDetailComponent implements OnInit {
  public brands_data:any = {
    name:'',
    title:'',
    content:'',
    logo:''
  };

  constructor(
    public route:ActivatedRoute,
    public service:BrandsService,
    public nes:NgxEchartsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      res => {
        this.getDetail(res['id']);
      }
    );
  }
  getDetail(id){
    this.service.getDetail({id:id}).subscribe(
      res => {
        if(res['code'] == 0){
          this.brands_data = res['data'];
          this.brands_data['logo'] = IMG_URL + JSON.parse(this.brands_data['logo'])[0];
          this.createPie();
        }
      }
    )
  }
  createPie(){
    let chartOption = {
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
      },
      legend: {
        orient: 'vertical',
        x: 'left',
        data:['网络人气','品牌价值','爱心公益','商业赞助','企业实力','与时俱进','幸福指数']
      },
      series: [
        {
          name:'榜单',
          type:'pie',
          radius: ['50%', '70%'],
          avoidLabelOverlap: false,
          label: {
            normal: {
              show: false,
              position: 'center'
            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: '30',
                fontWeight: 'bold'
              }
            }
          },
          labelLine: {
            normal: {
              show: false
            }
          },
          data:[
            {value:this.brands_data['Popularity'], name:'网络人气'},
            {value:this.brands_data['value'], name:'品牌价值'},
            {value:this.brands_data['Charity'], name:'爱心公益'},
            {value:this.brands_data['Support'], name:'商业赞助'},
            {value:this.brands_data['GProfit'], name:'企业实力'},
            {value:this.brands_data['Innovation'], name:'与时俱进'},
            {value:this.brands_data['Happiness'], name:'幸福指数'}
          ]
        }
      ]
    };

    let myCharts = this.nes.init(document.getElementsByClassName('echartsStyle')[0]);
    myCharts.setOption(chartOption);
  }
}
