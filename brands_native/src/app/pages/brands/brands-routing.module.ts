/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrandsListComponent} from "./brands-list/brands-list.component";
import {BrandsDetailComponent} from "./brands-detail/brands-detail.component";

// 子component

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: BrandsListComponent
            },
          {
            path: 'index/detail/:id',
            component: BrandsDetailComponent
          }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BrandsRoutingModule {}
/**
 * Created by qingyun on 2017/11/20.
 */
