import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class BrandsService {

  constructor( public http:HttpClient) {}

  // 获取数据
  getBanner(params): Observable<any> {
    return this.http.get(ROOT_URL + 'home/banner', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }
  //品牌列表百年品牌
  getBannerRank(params): Observable<any> {
    return this.http.get(ROOT_URL + 'brands/index', {params})
      .map(this.extractData)
      .catch(this.handleError);
  }
  //获取首页背景
  getBackImage(params):Observable<any>{
    return this.http.get(ROOT_URL + "background/index",{params})
      .map(this.extractData)
      .catch(this.handleError)
  }
  //品牌详情
  getDetail(params): Observable<any> {
    return this.http.get(ROOT_URL + 'brands/detail', {params})
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    return res;
  }
  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
