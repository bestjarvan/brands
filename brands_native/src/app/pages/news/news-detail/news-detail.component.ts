import {Component, ElementRef, OnInit} from '@angular/core';
import {NewsService} from "../news.service";
import index from "@angular/cli/lib/cli";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit {
  public newsDetail:object = {};
  public imgUrl:string = IMG_URL;
  constructor(public service:NewsService,
              public route: ActivatedRoute,
              public ele:ElementRef
  ) { }

  ngOnInit() {
    this.getNewsDetail();
    }
  getNewsDetail(){
    this.route.params.subscribe(params => {
      this.service.getNewDetail(params).subscribe(
        res =>{
          if(res['code'] == 0){
            this.newsDetail = res['data'];
            this.newsDetail['cover'] = JSON.parse(this.newsDetail['cover']);
            setTimeout(() => {
              let p = this.ele.nativeElement.querySelectorAll('#news-detail-content p');
              for(let i = 0;i < p.length;i++){
                p[i].style.marginBottom = '1rem';
              }
            },1);
          /*  this.newsDetail['content'] = this.newsDetail['content'].replace(/<[^<>]+>/g,'');*/
          }
        }
      )
    });



  }
}
