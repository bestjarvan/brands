/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewsListComponent} from "./news-list/news-list.component";
import {NewsDetailComponent} from "./news-detail/news-detail.component";

// 子component

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: NewsListComponent,
            },
            {
              path: 'index/detail/:id',
              component: NewsDetailComponent,
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewsRoutingModule {}
/**
 * Created by qingyun on 2017/11/20.
 */
