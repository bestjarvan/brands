import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class NewsService {

  constructor(public http:HttpClient) {};

  //获取数据
  getNewDate(params): Observable<any>{
    return this.http.get(ROOT_URL + 'news/index',{params})
      .map(this.extractData)
      .catch(this.handleError);

    }
  getNewRanking(params): Observable<any>{
    return this.http.get(ROOT_URL + 'news/newRanking',{params})
      .map(this.extractData)
      .catch(this.handleError);

  }
  getNewDetail(params): Observable<any>{
    return this.http.get(ROOT_URL + 'news/details',{params})
      .map(this.extractData)
      .catch(this.handleError);

  }
  private extractData(res: Response) {
    return res;
  }
  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
