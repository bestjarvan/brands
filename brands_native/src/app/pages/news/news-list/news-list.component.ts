import { Component, OnInit } from '@angular/core';
import {NewsService} from "../news.service";
import {LoadingService} from "../../../share/loading.service";

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  // 分页组件相关配置
  public totalItems: any ; // 列表数据总数
  public currentPage: any = 1; // 默认的开始页数
  public maxSize: Number = 3; // 显示的最大按钮数量

  public select:number = 0;
  public imgUrl:string = IMG_URL;
  public newsNav:Array<any> = [{name:'总览'},{name:"品牌快讯"},{name:"网站动态"},{name:"线下活动"},{name:"媒体报道"}];

  public newsList:Array<any> = [];

  public newsInformation:Array<any> = [];
  public no_more:boolean = false;
  public params:object = {
    type:1
  };

  constructor(public service:NewsService,
              public loading:LoadingService
              ) {

  }

  ngOnInit() {
    this.params['type'] = 1;
    this.getNewsData();
    this.getNewsRanking();
  }
  newsNavs(index){
    this.select = index;
    this.params['type'] = index + 1;
    this.getNewsData();
  }
  getNewsData(){
    this.loading.show();
    // type: 1首页  2品牌快讯 3公司动态 4市场活动 5媒体报道
    this.service.getNewDate(this.params).subscribe(
      res =>{
        if(res['code'] == 0){
          this.loading.hide();
          this.newsList = res['data']['data'];
          this.totalItems = res['data']['total'];
          if(this.newsList.length == 0){
            this.no_more = true;
          }else {
            this.no_more = false;
          }
          if(this.newsList){
            this.newsList.map((v) => {
              v['cover'] = JSON.parse(v['cover'])[0];
              v['content'] = v['content'].replace(/<[^<>]+>/g,'');
              v['content'] = v['content'].replace(/\s+/g,'');
              v['content'] = v['content'].substr(0,30) + '...';
            })
          }
        }
      }
    )

  }
  getNewsRanking(){
    const params = {
      type:1
    };
    this.service.getNewRanking(params).subscribe(
      res =>{
        if(res['code'] == 0){
          this.newsInformation = res['data'];
          if(this.newsInformation){
            this.newsInformation.map((v) =>{
              v['cover'] = JSON.parse(v['cover'])[0];
            })
          }
        }
    })
  }
  // 分页查询
  pageChanged(event) {
    this.currentPage = event.page;
    this.params['page'] = event.page;
    this.getNewsData();
  }

}
