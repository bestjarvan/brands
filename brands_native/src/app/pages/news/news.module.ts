import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsListComponent } from './news-list/news-list.component';
import {NewsRoutingModule} from "./news-routing.module";
import {FormsModule} from "@angular/forms";
import {ModalModule, PaginationModule} from "ngx-bootstrap";
import {NewsService} from "./news.service";
import { NewsDetailComponent } from './news-detail/news-detail.component';

@NgModule({
  imports: [
    CommonModule,
    NewsRoutingModule,
    FormsModule,
    PaginationModule,
    ModalModule.forRoot(),
  ],
  providers: [NewsService],
  declarations: [NewsListComponent, NewsDetailComponent]
})
export class NewsModule { }
