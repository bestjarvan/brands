import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {KSSwiperModule} from "angular2-swiper";
import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app.routing";
import {BsDropdownModule, PaginationModule} from "ngx-bootstrap";
import { TabsComponent } from './tabs/tabs.component';
import {BreadcrumbsComponent} from "./share/breadcrumb.component";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {LoadingService} from "./share/loading.service";
import {ProcessService} from "./share/process.service";
import {AlertService} from "./share/alert.service";
import {requestOptionsProvider} from "./default-request-options.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    KSSwiperModule,
    HttpClientModule,
    NgxEchartsModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    TabsComponent,
    BreadcrumbsComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  }, requestOptionsProvider, LoadingService, ProcessService, AlertService],
  bootstrap: [AppComponent]
})
export class AppModule { }
