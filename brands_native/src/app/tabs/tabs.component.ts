import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  public homeText:Array<any> = [{name:'首页',url:'/home'},{name:"品牌故事",url:'/brands'},{name:"动态信息",url:'/news'},{name:"联系我们",url:'/connect'}];
  public select:number = 0;
  //菜单
  public menuShow:boolean = false;
  public content:any;

  constructor(public router:Router) {

    window.onresize = () => {
      setTimeout(() => {
        if(window.innerWidth >= 870){
            this.menuShow = false;
        }
      })
    }

  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
    });
    for (let i = 0;i < this.homeText.length;i++){
      if(this.router.url.indexOf(this.homeText[i].url) >= 0){
        this.select = i;
      }
    }

    this.content = <HTMLElement>document.getElementsByClassName('container-content')[0];
  }

  clickName(){
    location.reload();
  }

  //首页导航
  menuToggle(){
    this.menuShow = !this.menuShow;
    if(this.menuShow){
      this.content.setAttribute('id', 'content-margin');
    }else {
      this.content.removeAttribute('id');
    }
  }
  //导航点击事件
  navBar(index,url){
    this.content.removeAttribute('id');
    this.menuShow = false;
    this.select = index;
  }


}
