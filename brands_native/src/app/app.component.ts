import { Component } from '@angular/core';
import {NavigationStart, Router} from "@angular/router";
import {LoadingService} from "./share/loading.service";

@Component({
  selector: 'body',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
  constructor(private router: Router,
              public loadingService:LoadingService,
              ){

  }
  ngOnInit(){
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.loadingService.isShow) {
          this.loadingService.hide();
        }
      }
    });
  }

}
