import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SelectivePreloadingStrategy} from "./app.preloading-strategy";
import {TabsComponent} from "./tabs/tabs.component";
// preloading
export const routes: Routes = [
  {
    path:"",
    redirectTo: "/home/index",
    pathMatch:"full"
  },
  {
    path: '',
    component: TabsComponent,
    children:[
      {
        path: 'home',
        loadChildren: './pages/home/home.module#HomeModule',
        data: {
          preload: true
        }
      },
      {
        path: 'brands',
        loadChildren: './pages/brands/brands.module#BrandsModule',
        data: {
          preload: true
        }
      },
      {
        path: 'news',
        loadChildren: './pages/news/news.module#NewsModule',
        data: {
          preload: true
        }
      },
      {
        path: 'connect',
        loadChildren: './pages/connect/connect.module#ConnectModule',
        data: {
          preload: true
        }
      }
    ]
  },




];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {preloadingStrategy:SelectivePreloadingStrategy})],
  exports: [ RouterModule ],
  providers: [SelectivePreloadingStrategy]
})
export class AppRoutingModule {}
