import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html'
})
export class FileInputComponent implements OnInit {
  @Input() public selectFile: EventEmitter<any> = new EventEmitter();
  @Output() public updateFile: EventEmitter<any> = new EventEmitter();
  constructor(public ele: ElementRef) {
  }
  ngOnInit() {
    this.selectFile.subscribe(() => {
      const fileInput = this.ele.nativeElement.querySelector('#fileUploadInput');
      fileInput.click();
      fileInput.addEventListener('change', () => {
        const file = fileInput.files[0];
        if (file) {
          const reader = new FileReader();
          reader.onload = () => {
            this.updateFile.emit(reader.result);
          };
          reader.readAsDataURL(file);
        }
      });
    });
  }
}

