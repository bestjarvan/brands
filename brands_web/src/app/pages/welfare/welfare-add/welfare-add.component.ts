import {Component, EventEmitter, OnInit, ViewChild} from '@angular/core';
import {WelfareService} from '../welfare.service';
import {LoadingService} from '../../../shared/loading.service';
import {ActivatedRoute} from '@angular/router';
import {AlertService} from '../../../shared/alert.service';
import {CropperComponent} from '../../../shared/cropper/cropper.component';

@Component({
  selector: 'app-welfare-add',
  templateUrl: './welfare-add.component.html',
  styleUrls: ['./welfare-add.component.scss']
})
export class WelfareAddComponent implements OnInit {
  @ViewChild(CropperComponent) private cropperComponent: CropperComponent;
  public editId: any;
  // 保存数据
  public editWelData: any = {
    title: '',
    id: '',
    content: '',
    // coverImg: '',
    type: 1,
  };
  public nameArr: Array<any> = [];

  // 图片是否上传
  public detailsPicture: Boolean = false;
  // 企业不可编辑
  public enterpriseRead: Boolean = false;
  // 保存上传图片
  public cropperImg: any;
  public receiveContent: EventEmitter<any> = new EventEmitter();
  public imgUrl = IMG_URL;
  public coverImg: Array<any> = [];
  public proofBo: Boolean = false;

  public uploadpictures: any  = '上传封面';

  constructor(
    public service: WelfareService,
    public loading: LoadingService,
    public alert: AlertService,
    public route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.service.getEnterprise({token: localStorage.token}).subscribe(
      res => {
        if (res['code'] == 0) {
          this.nameArr = res['data'];
        }
      }
    );
  }
  backPrevPage() {
    window.history.go(-1);
  }
  updateContent(obj) {
    this.editWelData.content = obj;
  }
  // 图片上传
  updateCroppersImg() {
    this.cropperComponent.show();
  }
  save() {

    if (!this.editWelData.title) {
      this.alert.show('新闻标题不能为空');
      return;
    }else if (this.editWelData.title.length > 20) {
      this.alert.show('新闻标题不能大于20个字符');
      return
    }
    if (!this.cropperImg) {
      this.alert.show('请上传封面');
      return;
    }
    if (!this.editWelData.id) {
      this.alert.show('请选择企业');
      return;
    }
    if (!this.editWelData.type) {
      this.alert.show('请选择新闻类型');
      return;
    }
    if (!this.editWelData.content) {
      this.alert.show('新闻内容不能为空');
      return;
    }
    const params: any = {
      token: localStorage.token,
      pid: this.editWelData.id,
      title: this.editWelData.title,
      content: this.editWelData.content,
      type: this.editWelData.type,
      releaseTime: Date.parse(String(new Date())),
      cover: this.cropperImg
    };
    this.service.addNews(params).subscribe(res => {
      if (res['code'] == 0) {
        this.alert.show('新增新闻成功');
        setTimeout(() => {
          window.history.go(-1);
        }, 100);
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {
      this.alert.show('网络错误');
    });
  }
//  获取图片
  commodityPictures (event) {
    this.cropperImg = event;
    this.uploadpictures = '重新上传';
    this.detailsPicture = true;
  }
//  删除图片
  deleteimg () {
    this.cropperImg = '';
    this.detailsPicture = false;
    this.uploadpictures = '上传封面';

  }

}
