import {Component, EventEmitter, OnInit, ViewChild} from '@angular/core';
import {WelfareService} from '../welfare.service';
import {LoadingService} from '../../../shared/loading.service';
import {AlertService} from '../../../shared/alert.service'
import {ActivatedRoute, Router} from '@angular/router';
import { CropperComponent } from '../../../shared/cropper/cropper.component';

@Component({
  selector: 'app-welfare-edit',
  templateUrl: './welfare-edit.component.html',
  styleUrls: ['./welfare-edit.component.scss']
})
export class WelfareEditComponent implements OnInit {
  @ViewChild(CropperComponent) private cropperComponent: CropperComponent;
  public editId: any;
  // 编辑的新闻id
  public newsId: any;
  public name: any;
  public uploadpictures: any  = '上传封面';
  // 图片是否上传
  public detailsPicture: Boolean = false;
  // 保存上传图片
  public cropperImg: any;
  // 保存数据
  public editWelData: any = {
    title: '',
    id: '',
    content: '',
    // coverImg: '',
    type: 1,
  };

  public receiveContent: EventEmitter<any> = new EventEmitter();
  public imgUrl = IMG_URL;

  constructor(
      public service: WelfareService,
      public loading: LoadingService,
      public alert: AlertService,
      public route: ActivatedRoute,
      public router: Router,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {

      this.name = params['name'];
      this.editId = params['id'];
      // 编辑新闻
      const params2: any = {
        token: localStorage.token,
        id: this.editId
      };
      this.service.detailNews(params2).subscribe(res => {
        if (res['code'] == 0) {
          this.newsId = res['data']['id'];
          this.editWelData.title = res['data']['title'];
          this.editWelData.id = res['data']['pid'];
          this.editWelData.type = res['data']['type'];
          this.editWelData.content = res['data']['content'];
          this.receiveContent.emit(res['data']['content']);
        }
      });

    });
  }
  backPrevPage() {
    tinymce.remove();
    window.history.go(-1);
  }

  //  获取图片
  commodityPictures (event) {
    this.cropperImg = event;
    this.uploadpictures = '重新上传';
    this.detailsPicture = true;
  }
//  删除图片
  deleteimg () {
    this.cropperImg = '';
    this.detailsPicture = false;
    this.uploadpictures = '上传封面';

  }
  // 图片上传
  updateCroppersImg() {
    this.cropperComponent.show();
  }
  updateContent(obj) {
    this.editWelData.content = obj;
  }
  save() {

    if (!this.editWelData.title) {
      this.alert.show('新闻标题不能为空');
      return;
    }else if (this.editWelData.title.length > 20) {
      this.alert.show('新闻标题不能大于20个字符');
      return
    }
    if (!this.editWelData.id) {
      this.alert.show('请选择企业');
      return;
    }
    if (!this.cropperImg) {
      this.alert.show('请上传封面');
      return;
    }
    if (!this.editWelData.type) {
      this.alert.show('请选择新闻类型');
      return;
    }
    if (!this.editWelData.content) {
      this.alert.show('新闻内容不能为空');
      return;
    }
    const params: any = {
      token: localStorage.token,
      pid: this.editWelData.id,
      title: this.editWelData.title,
      cover: this.cropperImg,
      id: this.newsId,
      content: this.editWelData.content,
      type: this.editWelData.type,
      releaseTime: Date.parse(String(new Date()))
    };
    this.service.editNews(params).subscribe(res => {
      if (res['code'] == 0) {
        this.alert.show('编辑新闻成功');
        setTimeout(() => {
          window.history.go(-1);
        }, 100);
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {
      this.alert.show('网络错误');
    });
  }
}
