import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {WelfareRoutingModule} from './welfare-routing.module';
import {WelfareService} from './welfare.service';

import { WelfareListComponent } from './welfare-list/welfare-list.component';
import { CropperModule } from '../../shared/cropper/cropper.module';
import { TinymceEditorModule} from '../../shared/tinymce-editor/tinymce-editor.module';
import { WelfareEditComponent } from './welfare-edit/welfare-edit.component';
import { WelfareAddComponent } from './welfare-add/welfare-add.component';
import {WelfareDetailComponent} from './welfare-detail/welfare-detail.component';


@NgModule({
  imports: [
    CommonModule,
    WelfareRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule,
    CropperModule,
    TinymceEditorModule,
  ],
  providers: [WelfareService],
  declarations: [WelfareListComponent, WelfareEditComponent, WelfareAddComponent, WelfareDetailComponent]
})
export class WelfareModule { }
