/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {WelfareListComponent} from './welfare-list/welfare-list.component';
import { WelfareEditComponent } from './welfare-edit/welfare-edit.component';
import {WelfareAddComponent} from './welfare-add/welfare-add.component';
import {WelfareDetailComponent} from './welfare-detail/welfare-detail.component';
const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: WelfareListComponent,
                data: {
                    title: '新闻管理'
                }
            },
            {
              path: 'index/add',
              component: WelfareAddComponent,
              data: {
                title: '新增新闻'
              }
            },
            {
                path: 'index/edit',
                component: WelfareEditComponent,
                data: {
                    title: '编辑新闻'
                }
            },
            {
                path: 'index/detail',
                component: WelfareDetailComponent,
                data: {
                    title: '查看新闻'
                }
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WelfareRoutingModule {}
