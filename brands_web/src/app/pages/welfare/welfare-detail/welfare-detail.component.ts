import { Component, EventEmitter , OnInit } from '@angular/core';
import {WelfareService} from '../welfare.service';
import {AlertService} from '../../../shared/alert.service';
import {LoadingService} from '../../../shared/loading.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-welfare-detail',
  templateUrl: './welfare-detail.component.html',
  styleUrls: ['./welfare-detail.component.scss']
})
export class WelfareDetailComponent implements OnInit {
  public imgUrl = IMG_URL;
  // 查看对象的数据存放
  public newsData: object = {
    title: '',
    enterprise: '',
    type: '',
    cover: '',
  };

  constructor(
      public service: WelfareService,
      public alert: AlertService,
      public loading: LoadingService,
      public route: ActivatedRoute
  ) { }

  ngOnInit() {
    const div = <HTMLElement>document.getElementById('newsContent');
    this.route.params.subscribe(params => {
      this.newsData = params;
      div.innerHTML = params['content'];
    });
  }


  backPrevPage() {
    history.go(-1);
  }

}
