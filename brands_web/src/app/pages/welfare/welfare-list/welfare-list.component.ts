import { Component, OnInit } from '@angular/core';
import {WelfareService} from '../welfare.service';
import {AlertService} from '../../../shared/alert.service';
import {LoadingService} from '../../../shared/loading.service';

@Component({
  selector: 'app-welfare-list',
  templateUrl: './welfare-list.component.html',
  styleUrls: ['./welfare-list.component.scss']
})
export class WelfareListComponent implements OnInit {
  // 列表数据存储
  public news: Array<any> = [];
  // 封面
  public imgUrl: string = IMG_URL;
  // 查询的数据存放
  public saveData: any = {
    key: '1',
    keywords: '',
  };

  // 存放上下架的状态
  public shelvesState: any;
  // 模态框文字
  public modalText: String;
  // id
  public welfareId: any;
  public welfareType: any;

    // 分页组件相关配置
    public totalItems: any ; // 列表数据总数
    public currentPage: any = 1; // 默认的开始页数
    public maxSize: Number = 3; // 显示的最大按钮数量
    public shelvesorDeletes: Boolean = false;

  constructor(
      public service: WelfareService,
      public alert: AlertService,
      public loading: LoadingService
  ) { }

  ngOnInit() {
    this.getNewsList();
    tinymce.remove();
  }

  // 获取公益管理列表
  getNewsList () {
    this.loading.show();
    const params: any = {
      token: localStorage.token,
      type: localStorage.type,
      page: this.currentPage,
      key: 1,
      keywords: ''
    };
    this.service.getNews(params).subscribe(
      res => {
        this.loading.hide();
        if (res['code'] === 0) {
          this.news = res['data']['data'];
          if (this.news) {
            this.news.map((v) => {
              v.cover = JSON.parse(v.cover);
            })
          }
          this.totalItems = res['data']['total'];
        }else {
          this.alert.show(res['msg']);
        }
      }, error => {
        this.alert.show('网络错误');
        this.loading.hide();
      }
    );
  }

  // 点击上/下架
    clickShelves(id, state, type) {
    this.shelvesState = state;
    if (this.shelvesState === 2) {
      this.modalText = '确定要上架吗？';
      this.shelvesState = 1;
    }else {
      this.modalText = '确定要下架吗？';
      this.shelvesState = 2;
    }
    this.welfareId = id;
    this.welfareType = type;
  }

    definiteRequest() {
    if (this.welfareType === 1) {
        // if (this.shelvesState === 1) {
        //     this.shelvesState = 2;
        // }else {
        //     this.shelvesState = 1;
        // }
      const params: any = {
        token: localStorage.token,
        id: this.welfareId,
        shelves: this.shelvesState
      };
      this.service.shelvesNews(params).subscribe(res => {
        if (res['code'] === 0) {
          if (this.shelvesState === 2) {
            this.alert.show('下架成功');
          }else {
            this.alert.show('上架成功');
          }
          this.getNewsList();
        }else {
          this.alert.show(res['msg']);
        }
      });
    }else {
      const params: any = {
        token: localStorage.token,
        id: this.welfareId,
        type: localStorage.type
      };
      this.service.deleteNews(params).subscribe(res => {
        if (res['code'] === 0) {
          this.alert.show('删除成功');
          this.getNewsList();
        }else {
          this.alert.show(res['msg']);
        }
      }, error => {
        this.alert.show('网络错误');
      });
    }
  }
  // 点击删除
  deleteitem(id, type) {
    this.welfareId = id;
    this.modalText = '确定要删除吗？';
    this.welfareType = type;
  }
  getNewsSearch() {
    this.loading.show();
    const params = {
      token: localStorage.token,
      type: localStorage.type,
      page: this.currentPage,
      key: this.saveData.key,
      keywords: this.saveData.keywords
    };
    this.service.getNews(params).subscribe(
      res => {
        this.loading.hide();
        if (res['code'] === 0) {
          this.news = res['data']['data'];
          this.totalItems = res['data']['total'];
        }else {
          this.alert.show(res['msg']);
        }
      }, error => {
        this.alert.show('网络错误');
        this.loading.hide();
      }
    );
  };

    // 分页查询
    pageChanged(event) {
        this.currentPage = event.page;
        this.getNewsList();
    }
}
