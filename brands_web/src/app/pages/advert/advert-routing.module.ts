/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {AdvertListComponent} from './advert-list/advert-list.component';
import {AdvertDetailComponent} from './advert-detail/advert-detail.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: AdvertListComponent,
                data: {
                    title: '背景图管理'
                }
            },
            {
                path: 'index/add',
                component: AdvertDetailComponent,
                data: {
                    title: '新增背景图'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdvertRoutingModule {}
