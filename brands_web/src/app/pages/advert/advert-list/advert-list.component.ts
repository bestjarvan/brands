import {Component, ElementRef, OnInit} from '@angular/core';
import {AdvertService} from '../advert.service';
import {LoadingService} from '../../../shared/loading.service';
import {AlertService} from '../../../shared/alert.service';

@Component({
  selector: 'app-advert-list',
  templateUrl: './advert-list.component.html',
  styleUrls: ['./advert-list.component.scss']
})
export class AdvertListComponent implements OnInit {

  // 分页组件相关配置
  public totalItems: any ; // 列表数据总数
  public currentPage: any = 1; // 默认的开始页数
  public maxSize: Number = 3; // 显示的最大按钮数量

  public dataArr: Array<any> = [];
  public imgUrl= IMG_URL;

  public shelvesorDeletes: Boolean = true;

  // 模态框
  public modalText: String = '';
  // 保存的上架/下架的状态
  public bannerDataState: any;

  public bannerDataId: any;
  public bannerDataHome: any;
  public bannerDataType: any;

  public dangerModal: Boolean = false;
  public successText: String;

  constructor(
    public loading: LoadingService,
    public service: AdvertService,
    public alert: AlertService
  ) { }

  ngOnInit() {
    this.getBannerData();
  }

//   获取轮播图数据
  getBannerData() {
    this.loading.show();
    const params = {
      token: localStorage.token,
      page: this.currentPage
    };
    this.service.getData(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0) {
        this.dataArr = res['data']['data'];
        if (this.dataArr) {
          // this.dataArr = res['data']['date'];
          this.dataArr.map((v, i) => {
            v['img'] = JSON.parse(v['img'])[0];
          } );
          this.totalItems = res['data']['num'];
        }else {
          this.dataArr = [];
        }
      }

    })
  }

  // 是否上架
  clickShelves(id, state, type , home) {
    if (state === 2) {
      this.modalText = '是否确定上架？';
      this.bannerDataState = 1;
    }else {
      this.modalText = '是否确定下架？';
      this.bannerDataState = 2;
    }
    this.bannerDataId = id;
    this.bannerDataType = type;
    this.bannerDataHome = home;
  }

  // 是否删除
  deleteitem(id, type) {
    this.bannerDataId = id;
    this.modalText = '您确定要删除吗？';
    this.bannerDataType = type;
  }
  // 确定/取消 上架
  definiteRequest() {
    if (this.bannerDataType === 1) {
      const params = {
        token : localStorage.token,
        id : this.bannerDataId,
        shelves : this.bannerDataState,
        type: this.bannerDataHome
      };
      this.service.setState(params).subscribe(res => {
        if (res['code'] === 0) {
          if (this.bannerDataState === 2) {
            this.alert.show('下架成功');
          }else {
            this.alert.show('上架成功');
          }
          this.getBannerData();
        }else if (res['code'] === 2008) {
          this.alert.show('只能上传5张');
        }else {
          this.alert.show(res['msg']);
        }
      });
    }else {
      const params = {
        token: localStorage.token,
        id: this.bannerDataId
      };
      this.service.bannerDelete(params).subscribe(res => {
        if (res['code'] === 0 ) {
          this.alert.show('删除成功');
          this.getBannerData();
        }
      });


    }
  }
  // 分页查询
  pageChanged(event) {
    this.currentPage = event.page;
    this.getBannerData();
  }


}
