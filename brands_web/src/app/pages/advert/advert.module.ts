import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {AdvertRoutingModule} from './advert-routing.module';
import {AdvertService} from './advert.service';

import { AdvertListComponent } from './advert-list/advert-list.component';
import { AdvertDetailComponent } from './advert-detail/advert-detail.component';
import {CropperModule} from '../../shared/cropper/cropper.module';
import {TinymceEditorModule} from '../../shared/tinymce-editor/tinymce-editor.module';


@NgModule({
  imports: [
    CommonModule,
    AdvertRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule,
    CropperModule,
    TinymceEditorModule,
  ],
  providers: [AdvertService],
  declarations: [AdvertListComponent, AdvertDetailComponent]
})
export class AdvertModule { }
