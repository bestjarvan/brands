/**
 * Created by qingyun on 2017/11/20.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component

import {HonorListComponent} from './honor-list/honor-list.component';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: HonorListComponent,
                data: {
                    title: '荣誉殿堂列表'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HonorRoutingModule {}

