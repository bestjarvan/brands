import { Component, OnInit } from '@angular/core';
import { LoadingService} from "../../../shared/loading.service";
import { AlertService} from "../../../shared/alert.service";
import { HonorService} from "../honor.service";
@Component({
  selector: 'app-honor-list',
  templateUrl: './honor-list.component.html',
  styleUrls: ['./honor-list.component.scss']
})
export class HonorListComponent implements OnInit {
  public honerListData: Array<any> = [];
   public isNoData: Boolean = false;
   public totalItems: Number;
   public currentPage: Number = 0;
   public maxSize: any = 3;
   public page: any;
   public searchHoner: any = {
       key: '1',
       keywords: ''
   };
  constructor(
      public alert: AlertService,
      public loading: LoadingService,
      public service: HonorService
  ) { }

  ngOnInit() {
    this.loading.show();
    this.getHonerLists();
  }
  getHonerLists() {
    this.isNoData = false;
    const params: any = {
      token: localStorage.token,
        page: this.page,
        key: this.searchHoner.key,
        keywords: this.searchHoner.keywords
    };
    this.service.getHonerList(params).subscribe(
        res => {
          this.loading.hide();
          if (res['code'] === 0) {
            this.honerListData = res['data']['data'];
            this.totalItems = res['data']['total'];
            if (this.honerListData.length == 0) {
              this.isNoData = true;
            }
          }
        }, error => {
          this.alert.show('网络错误');
          // window.location.reload();
    });
      // this.searchHoner.keywords = '';
  }
    pageChanged(event: any) {
        this.page = event.page;
        this.loading.show();
        this.getHonerLists();
    }

}
