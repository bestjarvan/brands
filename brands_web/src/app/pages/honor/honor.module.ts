import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';


import {HonorRoutingModule} from './honor-routing.module';
import {HonorService} from './honor.service';

import { HonorListComponent } from './honor-list/honor-list.component';

@NgModule({
  imports: [
    CommonModule,
    HonorRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  providers: [HonorService],
  declarations: [HonorListComponent]
})
export class HonorModule { }
