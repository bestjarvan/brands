import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {WithdrawalRoutingModule} from './withdrawal-routing.module';
import {WithdrawalService} from './withdrawal.service';

import { WithdrawalListComponent } from './withdrawal-list/withdrawal-list.component';
import { WithdrawalDetailComponent } from './withdrawal-detail/withdrawal-detail.component';
import { WithdrawalRemittanceComponent } from './withdrawal-remittance/withdrawal-remittance.component';

import { TinymceEditorModule } from '../../shared/tinymce-editor/tinymce-editor.module';


@NgModule({
  imports: [
    CommonModule,
    WithdrawalRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule,
      TinymceEditorModule
  ],
  providers: [WithdrawalService],
  declarations: [WithdrawalListComponent, WithdrawalDetailComponent, WithdrawalRemittanceComponent]
})
export class WithdrawalModule { }
