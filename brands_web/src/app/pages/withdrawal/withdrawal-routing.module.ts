/**
 * Created by qingyun on 2017/11/20.
 */
/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {WithdrawalListComponent} from './withdrawal-list/withdrawal-list.component';
import {WithdrawalRemittanceComponent} from './withdrawal-remittance/withdrawal-remittance.component';
import {WithdrawalDetailComponent} from './withdrawal-detail/withdrawal-detail.component';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: WithdrawalListComponent,
                data: {
                    title: '提现列表'
                }
            },
            {
                path: 'index/detail/:id',
                component: WithdrawalDetailComponent,
                data: {
                    title: '提现详情'
                }
            },
            {
                path: 'index/remittance/:id',
                component: WithdrawalRemittanceComponent,
                data: {
                    title: '汇款'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WithdrawalRoutingModule {}
