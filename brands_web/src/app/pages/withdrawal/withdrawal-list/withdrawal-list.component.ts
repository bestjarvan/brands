import { Component, ElementRef , OnInit } from '@angular/core';
import {WithdrawalService} from "../withdrawal.service";
import {LoadingService} from "../../../shared/loading.service";
import {AlertService} from "../../../shared/alert.service";

@Component({
  selector: 'app-withdrawal-list',
  templateUrl: './withdrawal-list.component.html',
  styleUrls: ['./withdrawal-list.component.scss']
})
export class WithdrawalListComponent implements OnInit {
    // 分页组件相关配置
    public totalItems: any ; // 列表数据总数
    public currentPage: any = 1; // 默认的开始页数
    public maxSize: Number = 3; // 显示的最大按钮数量

    public withDrawalData: Array<any> = [];
    public checkedData: any = {
      key: '1',
      keywords: '',
      state: ''
    };

  constructor(
      public service: WithdrawalService,
      public loading: LoadingService,
      public alert: AlertService,
      public ele: ElementRef
  ) { }

  ngOnInit() {
      this.getWithdrawalData();
  }
  // 提现列表
    getWithdrawalData() {
      // 通过DOM 的方法获取开始时间与结束时间
        const startTime = this.ele.nativeElement.querySelector('#start');
        const endTime = this.ele.nativeElement.querySelector('#end');
        if (startTime.value > endTime.value) {
            this.alert.show('申请开始时间必须小于结束时间');
            return;
        }
        this.loading.show();
      const params: any = {
          token: localStorage.token,
          key: this.checkedData.key,
          keywords: this.checkedData.keywords,
          startTime: startTime.value,
          endTime: endTime.value,
          page: this.currentPage,
          state: this.checkedData.state
      };
      this.service.withdrawalList(params).subscribe(res => {
          this.loading.hide();
          if (res['code'] === 0) {
              if (res['data']['data']) {
                  this.withDrawalData = res['data']['data'];
                  this.withDrawalData.map((v, i) => {
                      v['amount'] = v['amount'].slice(1);
                  });
                  this.totalItems = res['data']['total'];
              }else {
                  this.withDrawalData = [];
              }
          }else {
              // this.alert.show(res['msg']);
              this.withDrawalData = [];
          }
      }, error => {
          this.alert.show('网络错误');
      });
      // this.checkedData.key = '';
      // this.checkedData.keywords = '';
      // this.checkedData.state = '';
    }

  pageChanged(event) {
      this.currentPage = event.page;
      this.getWithdrawalData();
  }

}
