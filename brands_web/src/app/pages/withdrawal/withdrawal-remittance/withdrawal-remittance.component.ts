import { Component, EventEmitter , OnInit } from '@angular/core';
import {WithdrawalService} from '../withdrawal.service';
import {AlertService} from '../../../shared/alert.service';
import {LoadingService} from '../../../shared/loading.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-withdrawal-remittance',
  templateUrl: './withdrawal-remittance.component.html',
  styleUrls: ['./withdrawal-remittance.component.scss']
})
export class WithdrawalRemittanceComponent implements OnInit {
  public remId: String;
  public remDetailData: any = {
      account: '',
      name: '',
      nameBank: '',
      cardNumber: '',
      idNumber: '',
      tel: '',
      amount: '',
      addTime: '',
      state: '',
      description: ''
  };
  // 编辑器
    public receiveContent: EventEmitter<any> = new EventEmitter();
    public value: any;

  constructor(
      public service: WithdrawalService,
      public alert: AlertService,
      public loading: LoadingService,
      public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.remId = params['id'];
    });
    const params: any = {
      token: localStorage.token,
      id: this.remId
    };
    this.service.detailWithdrawal(params).subscribe(res => {
      if (res['code'] === 0) {
        this.remDetailData.account = res['data']['account'];
        this.remDetailData.name = res['data']['name'];
        this.remDetailData.cardNumber = res['data']['cardNumber'];
        this.remDetailData.idNumber = res['data']['idNumber'];
        this.remDetailData.tel = res['data']['tel'];
        this.remDetailData.amount = res['data']['amount'].slice(1);
        this.remDetailData.addTime = res['data']['addTime'];
        this.remDetailData.nameBank = res['data']['nameBank'];
        this.remDetailData.description = res['data']['description'];
        // this.receiveContent.emit(this.remDetailData.description);
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {
      this.alert.show('网络错误');
    });
  }
  // 保存按钮
    confirm() {
    if (!this.value) {
      this.alert.show('请选择汇款状态');
      return;
    }
    const params: any = {
      token: localStorage.token,
      id: this.remId,
      // amount: this.remDetailData.amount,
      // account: this.remDetailData.account,
      description: this.remDetailData.description,
      state: this.value
    };
    this.service.remittance(params).subscribe(res => {
      if (res['code'] === 0) {
        this.alert.show('汇款成功');
        setTimeout(() => {
          window.history.go(-1);
        }, 200);
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {
      this.alert.show('网络错误');
    });
  }




    backPrevPage() {
    window.history.go(-1);
    }
    // updateContent (obj) {
    //     this.remDetailData.description = obj;
    // }

}
