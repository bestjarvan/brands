import { Component, OnInit } from '@angular/core';
import {WithdrawalService} from "../withdrawal.service";
import {LoadingService} from "../../../shared/loading.service";
import {AlertService} from "../../../shared/alert.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-withdrawal-detail',
  templateUrl: './withdrawal-detail.component.html',
  styleUrls: ['./withdrawal-detail.component.scss']
})
export class WithdrawalDetailComponent implements OnInit {
  public detailId: String;
  public detailData: any;

  constructor(
      public service: WithdrawalService,
      public loading: LoadingService,
      public alert: AlertService,
      public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.detailId = params['id'];
    });
    const params: any = {
      token: localStorage.token,
      id: this.detailId
    };
    this.service.detailWithdrawal(params).subscribe(res => {
      this.detailData = res['data'];
      this.detailData['amount'] = res['data']['amount'].slice(1);
    });

  }
    returnButton() {
    window.history.go(-1);
    }

}
