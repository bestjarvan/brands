/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {UsersListComponent} from './users-list/users-list.component';
import {UsersDetailComponent} from './users-detail/users-detail.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: UsersListComponent,
                data: {
                    title: '用户列表'
                }
            },
            {
                path: 'index/detials/:id',
                component: UsersDetailComponent,
                data: {
                    title: '我的门生'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule {}
