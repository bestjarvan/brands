import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class UsersService {

  constructor( public http: Http) { }

  // 获取数据
  getData(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/index', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }


  // 获取门生
  getProtege(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/studentDetails', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }

  // 获取用户
  getUserD(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/studentMember', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }

  // 获取广告收入
  getAdincome(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/adIncome', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }


  // 获取销售收入
  getSaleincome(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/salesIncome', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }

  // 获取管理奖励
  getManageRewards(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/manageRewards', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }

  // 获取推荐人员
  getRecommended(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/recommend', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }
  private extractData(res: Response) {
    const body = res.json();
    if (body['code'] === 5003) {
      alert('账号在其他设备登录');
      localStorage.token = '';
      localStorage.tokenTime = '';
      // window.location.reload();
      // window.location.reload();
    }
    return body;
  }
  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
