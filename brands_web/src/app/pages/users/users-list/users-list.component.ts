import { Component, OnInit } from '@angular/core';
import {UsersService} from '../users.service';
import {LoadingService} from '../../../shared/loading.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  // 分页组件相关配置
  public totalItems: any ; // 列表数据总数
  public currentPage: any = 1; // 默认的开始页数
  public maxSize: Number = 3; // 显示的最大按钮数量

  public dataArr: Array<any> = [];

  public imgUrl = IMG_URL;

  public saveData: any = {
    key: '1',
    keywords: '',
    sex: ''
  };

  constructor(
      public service: UsersService,
      public loading: LoadingService
  ) { }

  ngOnInit() {
    this.getUser();
  }

//  获取数据
  getUser() {
    this.loading.show();
    const params = {
      token: localStorage.token,
      key: this.saveData.key,
      keywords: this.saveData.keywords,
      sex: this.saveData.sex,
      page: this.currentPage
    };
    this.service.getData(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0) {
        if (res['data']) {
          this.dataArr = res['data']['data'];
          this.dataArr.map((v, i) => {
            if (v['img']) {
              v['img'] = JSON.parse(v['img']);
            }
          });
          this.totalItems = res['data']['total'];
        }else {
          this.dataArr = [];
        }
      }else {
        this.dataArr = [];
      }
    });
  }

  // 分页查询
  pageChanged(event) {
    this.currentPage = event.page;
    this.getUser();
  }


}
