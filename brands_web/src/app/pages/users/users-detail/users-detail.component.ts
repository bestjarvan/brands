import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../../../shared/loading.service';
import {UsersService} from '../users.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss']
})
export class UsersDetailComponent implements OnInit {

  public detailId: any;
  public protegeData: Array<any> = [{
    data: '',
    children: ''
  }];

  public level: any = '' ;
  public levelArr: Array<any> = [];
  public imgUrl = IMG_URL;

  // 分页组件相关配置
  public totalItems: any ; // 列表数据总数
  public currentPage: any = 1; // 默认的开始页数
  public maxSize: Number = 3; // 显示的最大按钮数量

  // 五级数组
  public protegeLevel1: Array<any> = [];
  public protegeLevel2: Array<any> = [];
  public protegeLevel3: Array<any> = [];
  public protegeLevel4: Array<any> = [];
  public protegeLevel5: Array<any> = [];
  public protegeL1: Array<any> = [0];
  public protegeL2: Array<any> = [0, 1];
  public protegeL3: Array<any> = [0, 1, 2, 3];
  public protegeL4: Array<any> = [0, 1, 2, 3, 4, 5, 6, 7];
  public protegeL5: Array<any> = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

  // 层级
  public hierarchy: any;

  public detailListState: any;

  public userDetailData: any;
  public advertIncome: Array<any> = [];
  public saleIncome: Array<any> = [];
  public managementData: Array<any> = [];
  public recommendArr: Array<any> = [];

  constructor(
      public loading: LoadingService,
      public service: UsersService,
      public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.detailId = params['id'];
    });
    this.getProtegeData()
  }

//   获取门生数据
  getProtegeData() {
    this.protegeLevel1 = [];
    this.protegeLevel2 = [];
    this.protegeLevel3 = [];
    this.protegeLevel4 = [];
    this.protegeLevel5 = [];
    this.loading.show();
    const params = {
      token: localStorage.token,
      id: this.detailId
    };
    this.service.getProtege(params).subscribe(res => {
      this.loading.hide();


      // this.protegeData[0]['data'] = res['data']['studentTmp'];
      //
      // this.protegeData[0]['children'] = res['data']['list'];
      //


      if (res['code'] === 0) {
        if (res['data']['studentTmp']['img']) {
          res['data']['studentTmp']['img'] = JSON.parse(res['data']['studentTmp']['img'])[0];
        }
        this.protegeLevel1.push(res['data']['studentTmp']);
        this.hierarchy = res['data']['studentTmp']['grade'];
        this.protegeData = res['data']['list'];
        res['data']['list'].map((v, i) => {
          if (v['img']) {
            v['img'] = JSON.parse(v['img'])[0];
          }
          if (v['grade'] == this.hierarchy + 1) {
            this.protegeLevel2.push(v);
          }
          if (v['grade'] == this.hierarchy + 2) {
            this.protegeLevel3.push(v);
          }
          if (v['grade'] == this.hierarchy + 3) {
            this.protegeLevel4.push(v);
          }
          if (v['grade'] == this.hierarchy + 4) {
            this.protegeLevel5.push(v);
          }

        });

      }
    });
  }

  // 获取用户id
  clickDetail(obj: Object) {
    this.detailListState = -1;
    this.detailId = obj['id'];
    this.getProtegeData();
  }

  userDetail (state) {
    this.detailListState = state;
    this.loading.show();
    const params = {
      token: localStorage.token,
      id: this.detailId,
      grade: this.level,
      page: this.currentPage
    };
    switch (state) {
      case 1:
        this.service.getUserD(params).subscribe(res => {
          this.loading.hide();
          if (res['code'] === 0) {
            if (res['data']['img']) {
              res['data']['img'] = JSON.parse(res['data']['img'])[0];
            }
            this.userDetailData = res['data'];
          }
        });
        break;
      case 2:
        this.service.getAdincome(params).subscribe(res => {
          this.loading.hide();
          if (res['code'] === 0) {
            this.advertIncome = res['data'];
          }
        });
        break;
      case 3:
        this.service.getSaleincome(params).subscribe(res => {
          this.loading.hide();
          if (res['code'] === 0) {
            this.saleIncome = res['data'];
          }
        });
        break;
      case 4:
        this.service.getManageRewards(params).subscribe(res => {
          this.loading.hide();
          if (res['code'] === 0) {
            this.managementData = res['data'];
          }
        });
        break;
      case 5:
        this.service.getRecommended(params).subscribe(res => {
          this.loading.hide();
          if (res['code'] === 0) {
            if (res['data']['data']) {
              this.recommendArr = res['data']['data'];
              this.recommendArr.map((v, i) => {
                if (v['img']) {
                  v['img'] = JSON.parse(v['img'])[0];
                }
                this.levelArr.push(v['grade']);
              });
              if (this.levelArr.length) {
                this.levelArr = this.unique(this.levelArr);
              }
            }else {
              this.recommendArr = [];
            }
          }else {
            this.recommendArr = [];
          }
        });
        break;
      default:
        break;
    }
  }

  unique(arr) {
    const newArr = [arr[0]];
    for (let i = 1 ; i < arr.length; i++) {
      if (newArr .indexOf(arr[i]) == -1) {
        newArr.push(arr[i]);
      }
    }
    return newArr;
  }

  pageChanged(event) {
    this.currentPage = event.page;
    this.userDetail(5);
  }

  pageChanged1(event) {
    this.currentPage = event.page;
    this.userDetail(4);
  }

  pageChanged2(event) {
    this.currentPage = event.page;
    this.userDetail(3);
  }



}
