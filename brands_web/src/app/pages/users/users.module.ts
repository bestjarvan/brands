import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {UsersRoutingModule} from './users-routing.module';
import {UsersService} from './users.service';

import { UsersListComponent } from './users-list/users-list.component';
import { UsersDetailComponent } from './users-detail/users-detail.component';


@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  providers: [UsersService],
  declarations: [UsersListComponent, UsersDetailComponent]
})
export class UsersModule { }
