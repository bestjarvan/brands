/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {BannerListComponent} from './banner-list/banner-list.component';
import {BannerAddComponent} from './banner-add/banner-add.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: BannerListComponent,
                data: {
                    title: '轮播图列表'
                }
            },
            {
                path: 'index/add',
                component: BannerAddComponent,
                data: {
                    title: '新增轮播图'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BannerRoutingModule {}
/**
 * Created by qingyun on 2017/11/20.
 */
