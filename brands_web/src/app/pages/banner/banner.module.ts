import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {TinymceEditorModule} from '../../shared/tinymce-editor/tinymce-editor.module';
import {CropperModule} from '../../shared/cropper/cropper.module';

import {BannerRoutingModule} from './banner-routing.module';
import {BannerService} from './banner.service';

import { BannerListComponent } from './banner-list/banner-list.component';
import { BannerAddComponent } from './banner-add/banner-add.component';


@NgModule({
  imports: [
    CommonModule,
    BannerRoutingModule,
    FormsModule,
    PaginationModule,
    ModalModule.forRoot(),
    TinymceEditorModule,
    CropperModule
  ],
  providers: [BannerService],
  declarations: [BannerListComponent, BannerAddComponent]
})
export class BannerModule { }
