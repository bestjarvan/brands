/**
 * Created by zaq on 2017/5/12.
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class OrderService {
    constructor(private http: Http) {
    }
    // 获取列表
    bannerList(params): Observable<any> {
        return this.http.get(ROOT_URL +  'goods_order/index', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 详情
    bannerdetials(params): Observable<any> {
        return this.http.get(ROOT_URL + 'goods_order/details', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 导出列表
    getexcle(params): Observable<any> {
        return this.http.get(ROOT_URL + 'goods_order/get_excle', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 发货
    shipping(params): Observable<any> {
        return this.http.get(ROOT_URL + 'goods_order/shipping', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        const body = res.json();
        return body;
    }
    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
