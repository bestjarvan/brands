import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

// 分页组件
import {PaginationModule} from 'ngx-bootstrap';

// 组件
import { TinymceEditorModule } from '../../shared/tinymce-editor/tinymce-editor.module';
import {CropperModule} from '../../shared/cropper/cropper.module';
import {CommonModule} from '@angular/common';
import { OrderService } from './order.service';
import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order-list/order.component';
import { OrderDetialsComponent } from './order-detials/order-detials.component';

@NgModule({
    imports: [
        OrderRoutingModule,
        FormsModule,
        CommonModule,
        PaginationModule,
        ModalModule.forRoot(),
        TinymceEditorModule,
        CropperModule
    ],
    providers: [OrderService],
    declarations: [OrderComponent, OrderDetialsComponent]
})
export class OrderModule {
}
