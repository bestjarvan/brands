import {Component, ElementRef, OnInit} from '@angular/core';
import { OrderService } from '../order.service';
import { LoadingService } from '../../../shared/loading.service';
import { AlertService } from '../../../shared/alert.service';
@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit {
    public excelUrl: any = ROOT_URL + 'goods_order/get_excle?page=';
    public promptModal: Boolean = false;
    public smallModal: number;
    public imgurl: string;
    public imgurls: string;
    public page: string;
    public bannerList: Array<any> = [];
    // 判断是否有数据
    public isnoData: Boolean = false;
    // 保存图片请求头
    public IMG_URL: String = IMG_URL;
    // 分页组件相关配置
    public totalItems: any ; // 列表数据总数
    public currentPage: any = 1; // 默认的开始页数
    public maxSize: Number = 3; // 显示的最大按钮数量
    constructor(
        public ele: ElementRef,
        public service: OrderService,
        public loading: LoadingService,
        public alert: AlertService,
    ) {}
    ngOnInit() {
        this.loading.show();
        this.getData();
    }
    updateCropperImg(data) {
        this.imgurl = data;
    }
    updateCropperImgs(data) {
        this.imgurls = data;
    }
    openLoading() {
        this.loading.show();
    }
    pageChanged(event: any): void {
        // 切换分页进行网络请求
        this.page = event.page;
        this.loading.show();
        this.getData();
    }
    getData() {
        const params: any = {
            page: this.page,
        };
        this.service.bannerList(params).subscribe(
            data => {
                setTimeout( () => {
                    this.loading.hide();
                    if (data['data']['data']['length'] === 0) {
                        this.isnoData = true;
                        this.bannerList = [];
                    } else {
                        this.bannerList = data['data']['data'];
                        this.totalItems = data['data']['total'];
                        for (let i = 0; i < this.bannerList.length; i++ ) {
                            this.bannerList[i]['image'] = this.bannerList[i]['image'].replace(/"/g, '');
                            this.bannerList[i]['image'] = this.bannerList[i]['image'].substring(1, this.bannerList[i]['image']['length'] - 1);
                            this.bannerList[i]['image'] = this.bannerList[i]['image'].split(',');
                        }
                        this.isnoData = false;
                    }
                }, 300);
            },
            error => {
                this.loading.hide();
                this.alert.show('网络错误');
            }
        );
    }
    formatDateTime(inputTime) {
        const time = new Date( parseInt(inputTime + '000'));
        const y = time.getFullYear();
        const m = time.getMonth() + 1;
        const min = m < 10 ? ('0' + m) : m;
        const d = time.getDate();
        const day = d < 10 ? ('0' + d) : d;
        const h = time.getHours();
        const hours = h < 10 ? ('0' + h) : h;
        const minute = time.getMinutes();
        const second = time.getSeconds();
        const minutes = minute < 10 ? ('0' + minute) : minute;
        const seconds = second < 10 ? ('0' + second) : second;
        return y + '-' + min + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
    };
    deliverGoods(i) {
        this.smallModal = i;
        this.promptModal = true;
    }
    hideModal (i) {
        this.promptModal = false;
        if (i === 1) {
            this.loading.show();
            const params: any = {
                id: this.smallModal,
            };
            this.service.shipping(params).subscribe(
                res => {
                    this.loading.hide();
                    if (res['code'] === 0) {
                        this.alert.show('发货成功');
                        this.getData();
                    } else {
                        this.alert.show(res['msg']);
                    }
                },
                error => {
                    this.loading.hide();
                    this.alert.show('服务器繁忙');
                }
            );
        }
    }
    getexcles() {
       this.excelUrl = ROOT_URL + 'goods_order/get_excle?page=' + this.page;
    }
}

