import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../order.service';
import { LoadingService } from '../../../shared/loading.service';
import { AlertService } from '../../../shared/alert.service';

@Component({
  selector: 'app-order-detials',
  templateUrl: './order-detials.component.html',
  styleUrls: ['./order-detials.component.scss']
})
export class OrderDetialsComponent implements OnInit {
  public detialsData: any = {
      image: '',
      state: '',
  };
  public IMG_URL = IMG_URL;
  constructor(
      public loading: LoadingService,
      private route: ActivatedRoute,
      public service: OrderService,
      public alert: AlertService,
  ) { }

  ngOnInit() {
    this.loading.show();
    this.route.params.subscribe(params => {
        this.service.bannerdetials(params).subscribe(
            res => {
              if (res['code'] === 0) {
                this.detialsData = res['data'];
                this.detialsData['image'] = this.detialsData['image'].replace(/"/g, '');
                  this.detialsData['image'] = this.detialsData['image'].substring(1, this.detialsData['image']['length'] - 1);
                this.detialsData['image'] = this.detialsData['image'].split(',');
                this.loading.hide();
              }
        }
        );
    });
  }
    formatDateTime(inputTime) {
        const time = new Date( parseInt(inputTime + '000'));
        const y = time.getFullYear();
        const m = time.getMonth() + 1;
        const min = m < 10 ? ('0' + m) : m;
        const d = time.getDate();
        const day = d < 10 ? ('0' + d) : d;
        const h = time.getHours();
        const hours = h < 10 ? ('0' + h) : h;
        const minute = time.getMinutes();
        const second = time.getSeconds();
        const minutes = minute < 10 ? ('0' + minute) : minute;
        const seconds = second < 10 ? ('0' + second) : second;
        return y + '-' + min + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
    };
    back() {
        window.history.go(-1);
    }
}
