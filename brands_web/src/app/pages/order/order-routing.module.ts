import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import { OrderComponent } from './order-list/order.component';
import { OrderDetialsComponent } from './order-detials/order-detials.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'index',
        component: OrderComponent,
        data: {
          title: '订单列表'
        }
      },
        {
            path: 'index/detials/:id',
            component: OrderDetialsComponent,
            data: {
                title: '订单详情'
            }
        },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule {}
