import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {OpinionRoutingModule} from './opinion-routing.module';
import {OpinionService} from './opinion.service';

import { OpinionListComponent } from './opinion-list/opinion-list.component';

@NgModule({
  imports: [
    CommonModule,
    OpinionRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  providers: [OpinionService],
  declarations: [OpinionListComponent]
})
export class OpinionModule { }
