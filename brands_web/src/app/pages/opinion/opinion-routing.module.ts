/**
 * Created by qingyun on 2017/11/20.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component

import {OpinionListComponent} from './opinion-list/opinion-list.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: OpinionListComponent,
                data: {
                    title: '意见反馈'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OpinionRoutingModule {}

