import { Component, OnInit } from '@angular/core';
import {OpinionService} from '../opinion.service';
import {AlertService} from '../../../shared/alert.service';
import {LoadingService} from '../../../shared/loading.service';

@Component({
  selector: 'app-opinion-list',
  templateUrl: './opinion-list.component.html',
  styleUrls: ['./opinion-list.component.scss']
})
export class OpinionListComponent implements OnInit {
  // 意见反馈列表数据存放
  public opinionData: Array<any> = [];
  // 对象状态
  public opinStatus: Number;
  // 处理对象的Id
  public opinionId: String;
  // 分页组件
  public currentPage: any;
  public totalItems: any;
  public maxSize: Number = 3;
  public page: any;

  constructor(
      public service: OpinionService,
      public alert: AlertService,
      public loading: LoadingService
  ) { }

  ngOnInit() {
    this.getOpinionList();
  }
  // 意见反馈列表
  getOpinionList() {
    this.loading.show();
    const params: any = {
      token: localStorage.token,
      page: this.currentPage
    };
    this.service.opinionLists(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0) {
          this.opinionData = res['data']['data'];
          this.totalItems = res['data']['total'];
      }else {
        this.opinionData = [];
        this.alert.show(res['msg']);
      }
    }, error => {
      this.alert.show('网络错误')
    });
  }

  getId (id, status) {
    this.opinStatus = status;
    this.opinionId = id;
  }

  // 处理意见
    deal() {
    if (this.opinStatus == 1) {
      this.opinStatus = 2;
    }
    const params: any = {
      token: localStorage.token,
      id: this.opinionId,
      status: this.opinStatus
    };
    this.service.dealOpinion(params).subscribe(res => {
      if (res['code'] === 0) {
          this.alert.show('已处理');
          this.getOpinionList();
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {
      this.alert.show('网络错误');
    });
  }

    pageChanged(event) {
    this.currentPage = event.page;
    this.getOpinionList();
    }

}
