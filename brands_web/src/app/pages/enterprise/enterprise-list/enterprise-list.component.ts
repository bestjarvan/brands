import { Component, OnInit } from '@angular/core';
import {EnterpriseService} from '../enterprise.service';
import {LoadingService} from '../../../shared/loading.service';
import {AlertService} from '../../../shared/alert.service';

@Component({
  selector: 'app-enterprise-list',
  templateUrl: './enterprise-list.component.html',
  styleUrls: ['./enterprise-list.component.scss']
})
export class EnterpriseListComponent implements OnInit {
// 分页组件相关配置
  public totalItems: any ; // 列表数据总数
  public currentPage: any = 1; // 默认的开始页数
  public maxSize: Number = 3; // 显示的最大按钮数量

  public dataArr: Array<any> = [];
  public imgUrl= IMG_URL;

  public shelvesorDeletes: Boolean = true;

  //  查询 运营人员 数据存储
  public saveDate: any = {
    key: 1,
    keywords: ''
  };
  // 模态框
  public modalText: String = '';
  // 保存的上架/下架的状态
  public bannerDataState: any;

  public bannerDataId: any;
  public bannerDataHome: any;
  public bannerDataType: any;

  public dangerModal: Boolean = false;
  public successText: String;

  constructor(
      public service: EnterpriseService,
      public loading: LoadingService,
      public alert: AlertService
  ) { }

  ngOnInit() {
    this.getData();
  }
//   获取品牌数据数据
  getData() {
    this.loading.show();
    const params = {
      token: localStorage.token,
      type: localStorage.type,
      brands: 1
    };
    this.service.getData(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0) {
        this.dataArr = res['data']['data'];
        if (this.dataArr) {
          // this.dataArr = res['data']['date'];
          this.dataArr.map((v, i) => {
            v['logo'] = JSON.parse(v['logo'])[0];
          } );
          this.totalItems = res['data']['total'];
        }else {
          this.dataArr = [];
        }
      }

    })
  }

  // 是否上架
  clickShelves(id, state, type , home) {
    if (state === 2) {
      this.modalText = '是否确定上架？';
      this.bannerDataState = 1;
    }else {
      this.modalText = '是否确定下架？';
      this.bannerDataState = 2;
    }
    this.bannerDataId = id;
    this.bannerDataType = type;
    this.bannerDataHome = home;
  }

  // 是否删除
  deleteitem(id, type) {
    this.bannerDataId = id;
    this.modalText = '您确定要删除吗？';
    this.bannerDataType = type;
  }
  // 确定/取消 上架
  definiteRequest() {
    if (this.bannerDataType === 1) {
      const params = {
        token : localStorage.token,
        id : this.bannerDataId,
        shelves : this.bannerDataState
      };
      this.service.putEnterprise(params).subscribe(res => {
        if (res['code'] === 0) {
          if (this.bannerDataState === 2) {
            this.alert.show('下架成功');
          }else {
            this.alert.show('上架成功');
          }
          this.getData();
        }else {
          this.alert.show(res['msg']);
        }
      });
    }else {
      const params = {
        token: localStorage.token,
        id: this.bannerDataId
      };
      this.service.deleteEnterprise(params).subscribe(res => {
        if (res['code'] === 0 ) {
          this.alert.show('删除成功');
          this.getData();
        }
      });


    }
  }
  // 分页查询
  pageChanged(event) {
    this.currentPage = event.page;
    this.getData();
  }
  // 获取查询数据
  getPersonDate() {
    const params = {
      token: localStorage.token,
      type: localStorage.type,
      brands: 1,
      key: this.saveDate.key,
      keywords: this.saveDate.keywords
    };
    this.service.getData(params).subscribe(
      res => {
        this.loading.hide();
        if (res['code'] === 0) {
          this.dataArr = res['data']['data'];
          if (this.dataArr) {
            // this.dataArr = res['data']['date'];
            this.dataArr.map((v, i) => {
              v['logo'] = JSON.parse(v['logo'])[0];
            } );
            this.totalItems = res['data']['total'];
          }else {
            this.dataArr = [];
          }
        }
      }
    );
  }


}
