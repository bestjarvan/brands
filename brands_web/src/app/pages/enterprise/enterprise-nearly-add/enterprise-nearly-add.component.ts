import {Component, OnInit, ViewChild} from '@angular/core';
import {EnterpriseService} from '../enterprise.service';
import {LoadingService} from '../../../shared/loading.service';
import {AlertService} from '../../../shared/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CropperComponent} from '../../../shared/cropper/cropper.component';

@Component({
  selector: 'app-enterprise-nearly-add',
  templateUrl: './enterprise-nearly-add.component.html',
  styleUrls: ['./enterprise-nearly-add.component.scss']
})
export class EnterpriseNearlyAddComponent implements OnInit {
  @ViewChild(CropperComponent) private cropperComponent: CropperComponent;
// 图片是否上传
  public detailsPicture: Boolean = false;
  // 保存上传图片
  public cropperImg: any;
  // 按钮文字
  public uploadpictures: String = '上传图片并裁剪';
  public uploadpictures2: String = '删除图片';
  // 弹出框
  public isModalShown: Boolean = false;
  // 框的颜色
  public issuccess: Boolean = false;
  // 框的提示信息
  public modelBody: String;

  // 保存用户角色的信息
  public userRoles: Array<any> = [];

// 存放新增管理人员
  public newUsersData: any = {
    name: '',
    title: '',
    type: '准百年品牌',
    content: '',
    value: 0,
    Charity: 0,
    Support: 0,
    GProfit: 0,
    Innovation: 0,
    Happiness: 1,
  };


  constructor(public service: EnterpriseService,
              public loading: LoadingService,
              public alert: AlertService,
              public route: ActivatedRoute,
              public router: Router
  ) { }

  ngOnInit() {
    this.getSystems();
  }
  // 获取运营人员角色
  getSystems() {
    // this.loading.show();
    const params: any = {
      token: localStorage.token,
    };
    // this.service.getRoles(params).subscribe(res => {
    //   this.loading.hide();
    //   if (res['code'] === 0 ) {
    //     this.userRoles = res['data'];
    //   }
    // });
  }
  // 确定添加
  saveData() {
    if (!this.newUsersData.name) {
      this.alert.show('企业名称不能为空');
      return;
    }
    if (!this.newUsersData.title) {
      this.alert.show('品牌名全称不能为空');
      return;
    }
    if (!this.cropperImg) {
      this.alert.show('品牌LOGO不能为空');
      return;
    }
    this.loading.show();
    const params: any = {
      token: localStorage.token,
      name: this.newUsersData.name,
      title: this.newUsersData.title,
      type: 2,
      logo: this.cropperImg,
      content: this.newUsersData.content,
      value: this.newUsersData.value,
      Charity: this.newUsersData.Charity,
      Support: this.newUsersData.Support,
      GProfit: this.newUsersData.GProfit,
      Innovation: this.newUsersData.Innovation,
      Happiness: this.newUsersData.Happiness
    };
    this.service.addEnterprise(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0) {
        this.isModalShown = true;
        this.modelBody = '新增成功！';
        this.issuccess = true;
        this.alert.show('新增成功');
        setTimeout(() => {
          window.history.go(-1);
        }, 1000);
      }else {
        this.alert.show('新增失败 ' + res['msg']);
      }
    }, error => {
      this.loading.hide();
      this.alert.show('网络错误');
      // window.location.reload();
    });
  }
  // 取消添加
  cancle() {
    window.history.go(-1);
  }

  confirmshutdown () {
    this.loading.show();
    this.isModalShown = false;
    if (this.issuccess === true) {
      window.history.go(-1);
    }
    this.loading.hide();
  }
  // 图片上传
  updateCroppersImg() {
    this.cropperComponent.show();
  }
//  获取图片
  commodityPictures (event) {
    this.cropperImg = event;
    this.uploadpictures = '重新上传';
    this.detailsPicture = true;
  }
//  删除图片
  deleteimg () {
    this.cropperImg = '';
    this.detailsPicture = false;
    this.uploadpictures = '上传图片并裁剪';

  }
  // 幸福指数 1 ~ 100
  calcHappiness() {
    if (this.newUsersData.Happiness > 100) {
      this.newUsersData.Happiness = 100;
    }else if (this.newUsersData.Happiness <= 1) {
      this.newUsersData.Happiness = 1;
    }
    // this.newUsersData.Happiness = 1;
  }

}
