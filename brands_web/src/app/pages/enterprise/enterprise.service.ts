import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class EnterpriseService {

  constructor( public http: Http) { }

  // 获取品牌列表
  getData(params): Observable<any> {
    return this.http.get(ROOT_URL + 'Enterprise/index', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }
  // 添加企业
  addEnterprise(params): Observable<any> {
    return this.http.post(ROOT_URL + 'Enterprise/add', params)
        .map(this.extractData)
        .catch(this.handleError);
  }
  // 删除企业
  deleteEnterprise(params): Observable<any> {
    return this.http.delete(ROOT_URL + 'Enterprise/delete', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }
  // 上下架企业
  putEnterprise(params): Observable<any> {
    return this.http.put(ROOT_URL + 'Enterprise/shelves', params)
        .map(this.extractData)
        .catch(this.handleError);
  }
  // 企业详情
  goodsDetail(params): Observable<any> {
    return this.http.get(ROOT_URL + 'Enterprise/details', {params})
        .map(this.extractData)
        .catch(this.handleError);
  }
  // 企业详情修改
  goodsEdit(params): Observable<any> {
    return this.http.post(ROOT_URL + 'Enterprise/edit', params)
        .map(this.extractData)
        .catch(this.handleError);
  }



  private extractData(res: Response) {
    const body = res.json();
    // if (body['code'] === 5003) {
    //   alert('账号在其他设备登录');
    //   localStorage.token = '';
    //   localStorage.tokenTime = '';
      // window.location.reload();
    // }
    return body;
  }
  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
