import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {EnterpriseRoutingModule} from './enterprise-routing.module';
import {EnterpriseService} from './enterprise.service';
import {EnterpriseListComponent} from './enterprise-list/enterprise-list.component';
import {EnterpriseNearlyComponent} from './enterprise-nearly/enterprise-nearly.component';
import { EnterpriseAddComponent } from './enterprise-add/enterprise-add.component';
import {CropperModule} from '../../shared/cropper/cropper.module';
import { EnterpriseNearlyAddComponent } from './enterprise-nearly-add/enterprise-nearly-add.component';
import { EnterpriseEditComponent } from './enterprise-edit/enterprise-edit.component';




@NgModule({
  imports: [
    CommonModule,
    EnterpriseRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule,
    CropperModule
  ],
  providers: [EnterpriseService],
  declarations: [EnterpriseListComponent, EnterpriseNearlyComponent, EnterpriseAddComponent, EnterpriseNearlyAddComponent, EnterpriseEditComponent]
})
export class EnterpriseModule { }
