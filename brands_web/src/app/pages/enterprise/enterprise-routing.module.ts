/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {EnterpriseListComponent} from './enterprise-list/enterprise-list.component';
import {EnterpriseNearlyComponent} from './enterprise-nearly/enterprise-nearly.component';
import {EnterpriseAddComponent} from './enterprise-add/enterprise-add.component';
import {EnterpriseNearlyAddComponent} from './enterprise-nearly-add/enterprise-nearly-add.component';
import {EnterpriseEditComponent} from './enterprise-edit/enterprise-edit.component';


const routes: Routes = [
    {
        path: '',
        children: [
          {
            path: 'index',
            component: EnterpriseListComponent,
            data: {
              title: '百年品牌'
            }
          },
          {
            path: 'index/add',
            component: EnterpriseAddComponent,
            data: {
              title: '新增百年品牌'
            }
          },
          {
            path: 'index/edit/:id',
            component: EnterpriseEditComponent,
            data: {
              title: '编辑百年品牌'
            }
          },
          {
            path: 'nearly',
            component: EnterpriseNearlyComponent,
            data: {
              title: '准百年品牌'
            }
          },
          {
            path: 'nearly/add',
            component: EnterpriseNearlyAddComponent,
            data: {
              title: '新增准品牌'
            }
          },
          {
            path: 'nearly/edit/:id',
            component: EnterpriseEditComponent,
            data: {
              title: '编辑准百年品牌'
            }
          }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EnterpriseRoutingModule {}
