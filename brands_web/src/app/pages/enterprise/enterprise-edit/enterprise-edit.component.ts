import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EnterpriseService} from '../enterprise.service';
import {LoadingService} from '../../../shared/loading.service';
import {AlertService} from '../../../shared/alert.service';

@Component({
  selector: 'app-enterprise-edit',
  templateUrl: './enterprise-edit.component.html',
  styleUrls: ['./enterprise-edit.component.scss']
})
export class EnterpriseEditComponent implements OnInit {
  public id: any;
  public imgUrl: string = IMG_URL;
  public type: any = '加载中...';
  public enterpriseData: object = {
    name: '',
    logo: '',
    title: '',
    content: '',
    value: 0,
    Charity: 0,
    Support: 0,
    GProfit: 0,
    Innovation: 0,
    Happiness: 1,
  };

  constructor(
    public route: ActivatedRoute,
    public service: EnterpriseService,
    public loading: LoadingService,
    public alert: AlertService,
    public router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getData();
    })
  }

  // 获取企业详情数据
  getData() {
    const params = {
      token: localStorage.token,
      id: this.id
    };
    this.service.goodsDetail(params).subscribe(
      res => {
        if (res['code'] === 0) {
          this.enterpriseData = res['data'];
          if (this.enterpriseData['type'] == 1) {
            this.type = '百年品牌';
          }else {
            this.type = '准百年品牌';
          }
        } else {
          this.alert.show(res['msg']);
        }
      }
    )
  }
  imgJson(img) {
    return JSON.parse(img);
  }
  // 保存修改
  saveData() {
    this.enterpriseData['id'] = this.id;
    this.enterpriseData['token'] = localStorage.token;
    this.service.goodsEdit(this.enterpriseData).subscribe(
      res => {
        if (res['code'] === 0) {
          this.alert.show('修改成功!');
          setTimeout(() => {
            window.history.go(-1);
          }, 600);
        }else {
          this.alert.show(res['msg']);
        }
      }
    );
  }
  // 返回
  cancle() {
    window.history.go(-1);
  }

}
