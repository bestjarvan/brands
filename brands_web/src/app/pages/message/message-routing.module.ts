/**
 * Created by qingyun on 2017/12/4.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {MessageListComponent} from './message-list/message-list.component';



const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: MessageListComponent,
                data: {
                    title: '消息管理'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MessageRoutingModule {}