import { Component, OnInit } from '@angular/core';
import {MessageService} from '../message.service';
import {LoadingService} from '../../../shared/loading.service';
import {ActivatedRoute} from '@angular/router';
import {AlertService} from '../../../shared/alert.service';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit {
    // 分页组件相关配置
    public totalItems: any ; // 列表数据总数
    public currentPage: any = 1; // 默认的开始页数
    public maxSize: Number = 3; // 显示的最大按钮数量

    public messageData: Array<any> = [];
    public checkedData: any = {
      type: ''
    };
    public modalText: String;
    public deleteId: String;
    public shelvesorDeletes: Boolean = true;

  constructor(
      public service: MessageService,
      public loading: LoadingService,
      public alert: AlertService,
  ) { }

  ngOnInit() {
      this.getMessageLists();
  }
  getMessageLists() {
      this.loading.show();
      const params: any = {
          token: localStorage.token,
          page: this.currentPage,
          type: this.checkedData.type
      };
     this.service.messageLists(params).subscribe(res => {
         this.loading.hide();
         if (res['code'] === 0) {
             this.messageData = res['data']['data'];
             this.totalItems = res['data']['data']['total'];
             // this.checkedData.type = '';
         }else {
             this.messageData = [];
         }
     });
  }
  // 删除消息
    delete(id) {
      this.modalText = '确定要删除吗？';
      this.deleteId = id;
    }
    // 确定删除
    definiteRequest() {
      const params: any = {
          token: localStorage.token,
          id: this.deleteId
      };
      this.service.deleteMessage(params).subscribe(res => {
          if (res['code'] === 0) {
              this.alert.show('删除成功');
              this.getMessageLists();
          }else {
              this.alert.show(res['msg']);
          }
      }, error => {
          this.alert.show('网络错误');
      });
    }

  pageChanged(event) {
      this.currentPage = event.page;
      this.getMessageLists();
  }

}
