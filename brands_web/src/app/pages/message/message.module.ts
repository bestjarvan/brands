import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule , PaginationModule} from 'ngx-bootstrap';
import {MessageRoutingModule} from './message-routing.module';
import {MessageService} from './message.service';
import { FormsModule } from '@angular/forms';

import { MessageListComponent } from './message-list/message-list.component';

@NgModule({
  imports: [
    CommonModule,
    MessageRoutingModule,
      ModalModule.forRoot(),
      PaginationModule,
      FormsModule
  ],
  providers: [MessageService],
  declarations: [MessageListComponent]
})
export class MessageModule { }
