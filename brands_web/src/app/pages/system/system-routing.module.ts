/**
 * Created by qingyun on 2017/11/20.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {SystemPersonComponent} from './system-person/system-person.component';
import {SystemRolesComponent} from './system-roles/system-roles.component';
import {SystemPasswordComponent} from './system-password/system-password.component';
import {SystemPersonAddComponent} from './system-person-add/system-person-add.component';
import { SystemEditComponent } from './system-edit/system-edit.component';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: SystemPersonComponent,
                data: {
                    title: '运营人员列表'
                }
            },
            {
                path: 'index/add',
                component: SystemPersonAddComponent,
                data: {
                    title: '新增运营人员'
                }
            },
            {
                path: 'roles',
                component: SystemRolesComponent,
                data: {
                    title: '角色管理'
                }
            },

            {
                path: 'password',
                component: SystemPasswordComponent,
                data: {
                    title: '修改密码'
                }
            },
            {
                path: 'index/edit/:id',
                component: SystemEditComponent,
                data: {
                    title: '编辑运营人员'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SystemRoutingModule {}

