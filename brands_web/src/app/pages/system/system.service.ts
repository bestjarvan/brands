import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class SystemService {

    constructor( public http: Http) { }

    // 获取管理人员列表
    getPerson(params): Observable<any> {
        return this.http.get(ROOT_URL + 'User/roleList', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }

    // 获取角色管理列表
    getRoles(params): Observable<any> {
        return this.http.get(ROOT_URL + 'Roles/index', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 设置角色
    setRoles(params): Observable<any> {
        return this.http.post(ROOT_URL + 'Roles/update', params)
            .map(this.extractData)
            .catch(this.handleError);
    }

    // 角色管理添加
    addRoles(params): Observable<any> {
        return this.http.post(ROOT_URL + 'Roles/save', params)
            .map(this.extractData)
            .catch(this.handleError);
    }

    // 删除角色
   rolesDelete(params): Observable<any> {
        return this.http.delete(ROOT_URL + 'Roles/delete', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }

    // 获取角色管理 操作设置列表所有的权限
    getOperationRoles(params): Observable<any> {
        return this.http.get(ROOT_URL + 'Roles/authority', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 获取选择的权限信息
    operationRoles(params): Observable<any> {
        return this.http.get(ROOT_URL + 'Roles/one_authority', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }

    // 角色管理 添加用户权限
    setOperationRoles(params): Observable<any> {
        return this.http.post(ROOT_URL + 'Roles/operation', params)
            .map(this.extractData)
            .catch(this.handleError);
    }

    // 新增运营人员

    addSystemRoles(params): Observable<any> {
        return this.http.post(ROOT_URL + 'User/addSave', params)
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 编辑运营人员
    editSystemPer(params): Observable<any> {
        return this.http.post(ROOT_URL + 'User/edit', params)
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 删除运营人员
    delSystemPer(params): Observable<any> {
        return this.http.delete(ROOT_URL + 'User/delete', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 重置密码
    resetPassword(params): Observable<any> {
        return this.http.post(ROOT_URL + 'User/reset', params)
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 获取某个对象数据详情
    editPersonDetail(params): Observable<any> {
        return this.http.get(ROOT_URL + 'User/details', {params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    // 修改密码
    revisePassword(params): Observable<any> {
        return this.http.post(ROOT_URL + 'User/modifyPass', params)
            .map(this.extractData)
            .catch(this.handleError);
    }
  // 获取企业
  getEnterprise(params): Observable<any> {
    return this.http.get(ROOT_URL + 'User/enterprise', {params})
      .map(this.extractData)
      .catch(this.handleError);
  }

    private extractData(res: Response) {
        const body = res.json();
        if (body['code'] === 5003) {
            alert('账号在其他设备登录');
            localStorage.token = '';
            localStorage.tokenTime = '';
            // window.location.reload();
        }
        return body;
    }
    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}
