import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../../../shared/loading.service';
import {SystemService} from '../system.service';
import {AlertService} from '../../../shared/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-system-password',
  templateUrl: './system-password.component.html',
  styleUrls: ['./system-password.component.scss']
})
export class SystemPasswordComponent implements OnInit {

  public delectModal: Boolean = false;
  public issuccess: Boolean = false;
  public modelBody: string;
  // 保存填写内容
  public passwordData: any = {
      usedPass: '',
      newPass: '',
      confirmPass: '',
  };
  // 提示语控制
  public Isidentical: Boolean = false;
  public promptBool: Boolean = false;
  public validationHints: Boolean = false;

  constructor(
      public loading: LoadingService,
      private service: SystemService,
      public alert: AlertService,
      public router: Router
  ) { }
  // // 新旧密码验证
  // authenTication() {
  //   if (!this.modificationInformation.oldPassword) {
  //     this.promptBool = false;
  //     this.Isidentical = false;
  //     return;
  //   }
  //   if (!this.modificationInformation.newPassword) {
  //     this.promptBool = false;
  //     this.Isidentical = false;
  //     return;
  //   }
  //   if (this.modificationInformation.newPassword.length >= 6) {
  //     this.promptBool = false;
  //     this.Isidentical = false;
  //     return;
  //   }
  //   if (this.modificationInformation.newPassword === this.modificationInformation.oldPassword) {
  //     this.promptBool = false;
  //     this.Isidentical = true;
  //   } else {
  //     this.promptBool = true;
  //     this.Isidentical = false;
  //   }
  // }
  // // 两次新密码验证
  // inputValidation() {
  //   if (!this.modificationInformation.repassword) {
  //     this.validationHints = false;
  //     return;
  //   }
  //   if (this.modificationInformation.repassword === this.modificationInformation.newPassword) {
  //     this.validationHints = false;
  //   } else {
  //     this.validationHints = true;
  //   }
  // }
  ngOnInit() {
  }
  //
  // modifySubmit () {
  //   if (!this.modificationInformation.oldPassword) {
  //     this.alert.show('旧密码不能为空！');
  //     return;
  //   }
  //   if (!this.modificationInformation.oldPassword) {
  //     this.alert.show('新密码不能为空！');
  //     return;
  //   }
  //   if (!this.modificationInformation.repassword) {
  //     this.alert.show('确认密码不能为空！');
  //     return;
  //   }
  //   if (this.modificationInformation.newPassword === this.modificationInformation.oldPassword) {
  //     this.alert.show('新密码和旧密码不能一致！');
  //   } else if (this.modificationInformation.newPassword === this.modificationInformation.repassword) {
  //
  //     if (this.modificationInformation.newPassword.length >= 6) {
  //       const params: any = {
  //         token: localStorage.token,
  //         type: localStorage.type,
  //         clearly: this.modificationInformation.newPassword,
  //         original: Md5.hashStr(this.modificationInformation.oldPassword),
  //         password: Md5.hashStr(this.modificationInformation.newPassword),
  //         confirm: Md5.hashStr(this.modificationInformation.repassword),
  //       };
  //       this.service.modifyPassword(params).subscribe(
  //           res => {
  //             if (res['code'] === 0) {
  //               this.alert.show('修改成功！');
  //               setTimeout(() => {
  //                 this.router.navigate(['/login']);
  //               }, 2000);
  //             } else {
  //               this.delectModal = true;
  //               this.issuccess = false;
  //               this.modelBody = res['msg'];
  //             }
  //           },
  //           error => {
  //             this.loading.hide();
  //             this.alert.show('网络错误');
  //           }
  //       );
  //     }else {
  //       this.alert.show('新密码不够6位数');
  //     }
  //   } else {
  //     this.alert.show('两次密码不一致');
  //   }
  // }
  //
  //
  // confirmshutdown() {
  //   this.delectModal = false;
  // }

    savePwd() {

      if (!this.passwordData.usedPass) {
            this.alert.show('旧密码不能为空！');
            return;
          }
      if (!this.passwordData.newPass) {
        this.alert.show('新密码不能为空！');
        return;
      }
      if (this.passwordData.newPass.length < 6) {
        this.alert.show('请输入6-20位的有效新密码');
        return;
      }
      if (this.passwordData.usedPass === this.passwordData.newPass) {
        this.alert.show('新密码和旧密码不能相同，请重新输入');
        return;
      }
      if (!this.passwordData.confirmPass) {
        this.alert.show('确认密码不能为空！');
        return;
      }


    if (this.passwordData.newPass !== this.passwordData.confirmPass) {
      this.alert.show('两次输入的新密码不一致');
      return;
    }
        this.loading.show();

        const params: any = {
      token: localStorage.token,
      id: localStorage.id,
      usedPass: this.passwordData.usedPass,
      newPass: this.passwordData.newPass,
      confirmPass: this.passwordData.confirmPass
    };
    this.service.revisePassword(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0) {
        this.alert.show('修改密码成功');
        setTimeout(() => {
          // this.router.navigateByUrl['/login'];
            this.router.navigate(['/login']);
        }, 1000);
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {
      this.alert.show('网络有误');
      // window.location.reload();
    });
  }

    cancle() {
    history.go(-1);
    }
    keyUp () {
        this.passwordData.usedPass = this.passwordData.usedPass.replace(/[\W]/g, '');
        this.passwordData.newPass = this.passwordData.newPass.replace(/[\W]/g, '');
        this.passwordData.confirmPass = this.passwordData.confirmPass.replace(/[\W]/g, '');
    }

}
