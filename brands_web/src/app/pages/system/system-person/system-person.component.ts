import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../../../shared/loading.service';
import {SystemService} from '../system.service';
import {AlertService} from '../../../shared/alert.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-system-person',
  templateUrl: './system-person.component.html',
  styleUrls: ['./system-person.component.scss']
})
export class SystemPersonComponent implements OnInit {
  // 运营人员数据存储
    public personDataArr: Array<any> = [];

    public rolesDataArr: Array<any> = [];

  //  查询 运营人员 数据存储
  public saveDate: any = {
      key: '1',
      account: '',
      name: ''
  };
  // 模态框
    public dangerModal: Boolean = false;
    public resetOrDel: Boolean = false;
    public issuccess: Boolean = false;
    // 要删除的id
    public delId: String;
    // 第一个提示框文字
    public modalText: String;
    // 第二个提示框文字
    public successText: String;
    // 重置的新密码
    public rePassword: String;
    // 确认重置密码
    public reConfirmPass: String;
    // 分页组件
    // public page: any;
    public currentPage: any = 1;
    public maxSize: Number = 3;
    public totalItems: any;
    // 要重置密码的id
    public resetId: String;
    public resetPass: String;
    public resetConfirm: String;

    public personId: any;

  constructor(
      public loading: LoadingService,
      public service: SystemService,
      public alert: AlertService,
      public routes: ActivatedRoute
  ) { }

  ngOnInit() {
      this.getPersonDate();
      this.getRole();
  }
//  获取管理人员列表
  getPersonDate() {
    if (this.saveDate.key == 1) {
      this.saveDate.name = '';
    }else {
      this.saveDate.account = '';
    }
    this.loading.show();
    const params = {
        token: localStorage.token,
        key: this.saveDate.key,
        account: this.saveDate.account,
        name: this.saveDate.name,
        // page: this.page
    };

    this.service.getPerson(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] == 0) {
          this.personDataArr = res['data']['data'];
          this.totalItems = res['data']['data']['total'];
          this.personDataArr.map((v, i) => {
              // v['image_url'] = JSON.parse(v['image_url'])[0];
              if (v['time']) {
                  v['time'] = new Date( parseInt (v['time']) * 1000).toLocaleString().replace(/年|月/g, '-').replace(/日/g, ' ');
              }
          } );

      }
    });
  }

  // 获取权限
    getRole() {
        this.loading.show();
        const params = {
            token: localStorage.token,
        };
        this.service.getRoles(params).subscribe(res => {
            if (res['code'] === 0) {
                this.rolesDataArr = res['data'];
            }
        }, error => {
            this.alert.show('网络错误');
            // window.location.reload();
        });
    }

  // 点击删除按钮
    deleteUser (id) {
      this.delId = id;
      this.modalText = '你确定要删除吗？';
    }
    // 确定删除
    defineOperate () {
            const params: any = {
                token: localStorage.token,
                id: this.delId
            };
            this.service.delSystemPer(params).subscribe(res => {
                if (res['code'] === 0) {
                    this.alert.show('删除成功');
                    this.getPersonDate();
                }else {
                    this.alert.show('删除失败');
                    this.getPersonDate();
                }
            }, error => {
                this.alert.show('网络错误');
                // window.location.reload();
            });
    }

    hideModal() {
      this.loading.show();
      this.dangerModal = false;
      this.getPersonDate();
    }
    // 获取要重置对象的数据
    getPassData(id) {
        this.personId = id;
    }
    // 确定密码重置
    addmodify() {
      if (!this.resetPass) {
          this.alert.show('重置密码不能为空');
          return;
      }
      if (this.resetPass.length < 6) {
        this.alert.show('密码不能少于6位');
      }
        if (this.resetPass !== this.resetConfirm) {
            this.alert.show('确认重置密码和重置密码不一致');
            return;
        }
      const params: any = {
          token: localStorage.token,
          id: this.personId,
          password: this.resetPass,
          confirmPass: this.resetConfirm
      };
      this.service.resetPassword(params).subscribe(res => {
          if (res['code'] === 0) {
              // this.dangerModal = true;
              // this.issuccess = true;
              // this.successText = '重置密码成功';
              this.alert.show('重置密码成功');
              this.resetConfirm = '';
              this.resetPass = '';
              this.getPersonDate();
          }else {
              // this.dangerModal = false;
              // this.successText = '重置密码失败';
              // this.issuccess = false;
              this.alert.show(res['msg']);

          }
      }, error => {});
    }

    pageChanged (event) {
      this.currentPage = event.page;
      this.loading.show();
      this.getPersonDate();
    }

}
