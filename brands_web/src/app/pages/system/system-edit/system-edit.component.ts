import { Component, OnInit } from '@angular/core';
import {SystemService} from '../system.service';
import {AlertService} from '../../../shared/alert.service';
import {LoadingService} from '../../../shared/loading.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-system-edit',
  templateUrl: './system-edit.component.html',
  styleUrls: ['./system-edit.component.scss']
})
export class SystemEditComponent implements OnInit {
  public nameChange: String;
  public accountChange: String;
  public typeChange: any;
  public editId: String;

  public allData: Array<any> = [];

  public usersType: Array<any> = [];
  public nameArr: Array<any> = [];

  // 企业id
  public eid: any;

  constructor(
      public service: SystemService,
      public alert: AlertService,
      public loading: LoadingService,
      public routes: ActivatedRoute
  ) { }

  ngOnInit() {
    this. getRolesType();
    this.routes.params.subscribe( params => {
      this.editId = params['id'];
      });
    const params: any = {
      token: localStorage.token,
      id: this.editId
    };
    this.service.editPersonDetail(params).subscribe(res => {
      this.accountChange = res['data']['account'];
      this.nameChange = res['data']['name'];
      this.typeChange = res['data']['type'];
    }, error => {});

    this.service.getEnterprise({token: localStorage.token}).subscribe(
      res => {
        if (res['code'] == 0) {
          this.nameArr = res['data'];
        }
      }
    );
  }

  // 获取角色类型
  getRolesType() {
    const params: any = {
      token: localStorage.token
    };
    this.service.getRoles(params).subscribe(res => {
      this.usersType = res['data'];
    });
  }
  // 取消按钮
  return () {
    window.history.go(-1);
  }

  // 确认修改
  saveEdit () {
    const params: any = {
      token: localStorage.token,
      id: this.editId,
      name: this.nameChange,
      account: this.accountChange,
      type: this.typeChange,
      eid: this.eid ? this.eid : '',
    };
    this.service.editSystemPer(params).subscribe(res => {
      if (res['code'] === 0) {
        this.alert.show('修改成功');
        setTimeout(() => {
          window.history.go(-1);
        }, 300);
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {});
  }


}
