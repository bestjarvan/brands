import { Component, OnInit } from '@angular/core';
import {SystemService} from '../system.service';
import {LoadingService} from '../../../shared/loading.service';
import {AlertService} from '../../../shared/alert.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-system-person-add',
  templateUrl: './system-person-add.component.html',
  styleUrls: ['./system-person-add.component.scss']
})
export class SystemPersonAddComponent implements OnInit {

  // 弹出框
  public isModalShown: Boolean = false;
  // 框的颜色
  public issuccess: Boolean = false;
  // 框的提示信息
  public modelBody: String;

  // 保存用户角色的信息
  public userRoles: Array<any> = [];
  // 企业名字
  public nameArr: Array<any> = [];

// 存放新增管理人员
  public newUsersData: any = {
    account: '',
    name: '',
    type: '1',
    password: '',
    confirmPass: '',
    eid: ''
  };
  // 用户角色类型
  public usersType: any = [];

  constructor(
      public service: SystemService,
      public loading: LoadingService,
      public alert: AlertService,
      public route: ActivatedRoute,
      public router: Router,
  ) { }

  ngOnInit() {
    this.getSystems();

    this.service.getEnterprise({token: localStorage.token}).subscribe(
      res => {
        if (res['code'] == 0) {
          this.nameArr = res['data'];
        }
      }
    );
  }
  // 获取运营人员角色
  getSystems() {
    this.loading.show();
    const params: any = {
      token: localStorage.token,
    };
    this.service.getRoles(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0 ) {
        this.userRoles = res['data'];
      }
    });
  }
  // 确定添加
    saveData() {
    if (!this.newUsersData.account) {
      this.alert.show('账号不能为空');
      return;
    }
    if (!this.newUsersData.name) {
      this.alert.show('姓名不能为空');
      return;
    }
    if (!this.newUsersData.password) {
      this.alert.show('密码不能为空');
      return;
    }
    if (this.newUsersData.password.length < 6 ) {
      this.alert.show('密码不能小于6位');
      return;
    }
    if (this.newUsersData.password !== this.newUsersData.confirmPass) {
      this.alert.show('密码和确认密码不相同');
      return;
    }
    this.loading.show();
    const params: any = {
      token: localStorage.token,
      account: this.newUsersData.account,
      name: this.newUsersData.name,
      password: this.newUsersData.password,
      confirmPass: this.newUsersData.confirmPass,
      type: this.newUsersData.type,
      eid: this.newUsersData.eid ? this.newUsersData.eid : '',
    };
    this.service.addSystemRoles(params).subscribe(res => {
      this.loading.hide();
      if (res['code'] === 0) {
        // this.isModalShown = true;
        // this.modelBody = '新增成功！';
        // this.issuccess = true;
        this.alert.show('新增成功');
        setTimeout(() => {
          window.history.go(-1);
        }, 200);
      }else {
        this.alert.show(res['msg']);
      }
    }, error => {
      this.loading.hide();
      this.alert.show('网络错误');
      // window.location.reload();
    });
  }
  // 取消添加
    cancle() {
    window.history.go(-1);
    }

    confirmshutdown () {
        this.loading.show();
        this.isModalShown = false;
        if (this.issuccess === true) {
            window.history.go(-1);
        }
        this.loading.hide();
    }






}
