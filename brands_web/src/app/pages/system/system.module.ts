import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';

import {SystemRoutingModule} from './system-routing.module';
import {SystemService} from './system.service';

import { SystemPersonComponent } from './system-person/system-person.component';
import { SystemRolesComponent } from './system-roles/system-roles.component';
import { SystemPasswordComponent } from './system-password/system-password.component';
import { SystemPersonAddComponent } from './system-person-add/system-person-add.component';
import { SystemEditComponent } from './system-edit/system-edit.component';



@NgModule({
  imports: [
    CommonModule,
    SystemRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  providers: [SystemService],
  declarations: [SystemPersonComponent, SystemRolesComponent, SystemPasswordComponent, SystemPersonAddComponent, SystemEditComponent]
})
export class SystemModule { }
