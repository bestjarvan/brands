import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {GameRoutingModule} from './game-routing.module';
import {GameService} from './game.service';


import { GameListComponent } from './game-list/game-list.component';
import { GameAddComponent } from './game-add/game-add.component';


@NgModule({
  imports: [
    CommonModule,
    GameRoutingModule,
    PaginationModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  providers: [GameService],
  declarations: [GameListComponent, GameAddComponent]
})
export class GameModule { }
