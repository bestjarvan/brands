import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// 子component
import {GameListComponent} from './game-list/game-list.component';
import {GameAddComponent} from './game-add/game-add.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'index',
                component: GameListComponent,
                data: {
                    title: '游戏素材'
                }
            },
            {
                path: 'index/add',
                component: GameAddComponent,
                data: {
                    title: '游戏素材详情'
                }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GameRoutingModule {}
