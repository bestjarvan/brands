import { Component, OnInit } from '@angular/core';
import {GameService} from '../game.service';
import {LoadingService} from '../../../shared/loading.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {
    // 分页组件相关配置
    public totalItems: any ; // 列表数据总数
    public currentPage: any = 1; // 默认的开始页数
    public maxSize: Number = 3; // 显示的最大按钮数量
  constructor(
      public service: GameService,
      public loading: LoadingService
  ) { }

  ngOnInit() {
  }
  // 获取游戏数据
  getGameData () {
    this.loading.show();
  }

    pageChanged(event) {
      this.currentPage = event.page;
      this.getGameData();
    }

}
