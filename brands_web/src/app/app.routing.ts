import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';

// preloading
import { SelectivePreloadingStrategy } from './app.preloading-strategy';
import {LoginComponent} from './pages/login/login.component';

const permissions: any = localStorage.permissions ? JSON.parse(localStorage.permissions) : [{controller: ''}];
const redirectUrl: any = permissions[0].controller ? permissions[0].controller + '/index' : 'login';
const route_login: any = {
  path: '',
  redirectTo: redirectUrl,
  pathMatch: 'full'
};

export const routes: Routes = [
    {
      path: 'login',
      component: LoginComponent,
    },
    route_login,
    {
      path: '',
      component: FullLayoutComponent,
      data: {
        title: '后台管理中心'
      },
      children: [
        // {
        //   path: 'users',
        //   loadChildren: './pages/users/users.module#UsersModule',
        //   data: {
        //     preload: true
        //   }
        // },
        {
          path: 'banner',
          loadChildren: './pages/banner/banner.module#BannerModule',
          data: {
            preload: true
          }
        },
        {
          path: 'advert',
          loadChildren: './pages/advert/advert.module#AdvertModule',
          data: {
            preload: true
          }
        },
        {
          path: 'enterprise',
          loadChildren: './pages/enterprise/enterprise.module#EnterpriseModule',
          data: {
            preload: true
          }
        },
        {
          path: 'welfare',
          loadChildren: './pages/welfare/welfare.module#WelfareModule',
          data: {
            preload: true
          }
        },
        // {
        //   path: 'honor',
        //   loadChildren: './pages/honor/honor.module#HonorModule',
        //   data: {
        //     preload: true
        //   }
        // },
        // {
        //   path: 'game',
        //   loadChildren: './pages/game/game.module#GameModule',
        //   data: {
        //     preload: true
        //   }
        // },
        // {
        //   path: 'withdrawal',
        //   loadChildren: './pages/withdrawal/withdrawal.module#WithdrawalModule',
        //   data: {
        //     preload: true
        //   }
        // },
        // {
        //   path: 'message',
        //   loadChildren: './pages/message/message.module#MessageModule',
        //   data: {
        //     preload: true
        //   }
        // },
        // {
        //   path: 'opinion',
        //   loadChildren: './pages/opinion/opinion.module#OpinionModule',
        //   data: {
        //     preload: true
        //   }
        // },
        {
          path: 'system',
          loadChildren: './pages/system/system.module#SystemModule',
          data: {
            preload: true
          }
        },
      ]
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { preloadingStrategy: SelectivePreloadingStrategy })],
  exports: [ RouterModule ],
  providers: [SelectivePreloadingStrategy]
})
export class AppRoutingModule {}
