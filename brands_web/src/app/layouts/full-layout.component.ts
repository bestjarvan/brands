import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {NavigationEnd, Router} from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {
    public userName: any = localStorage.userName;
    public disabled = false;
    public status: {isopen: boolean} = {isopen: false};
    public permissions: any = localStorage.permissions ? JSON.parse(localStorage.permissions) : [];
    public isShowMsgPoint: any = false;
    public url: string;
    public toggled(open: boolean): void {
    }
    public toggleDropdown($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }
    constructor(public http: Http, public router: Router) {};

    ngOnInit(): void {
            this.url = '/' + JSON.parse(localStorage.permissions)[0]['controller'] + '/index';
            this.router.events.subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    if (event['url'] === '/message/index') {
                        this.isShowMsgPoint = false;
                        return;
                    }
                    const params: any = {
                        token: localStorage.token,
                    };
                    // this.http.get(ROOT_URL + '', {params}).subscribe((result) => {
                    //     result = result.json();
                    //     for (let i = 0; i < result['data']['data']['length']; i++) {
                    //         if (result['data']['data'][i]['state'] === 2 ) {
                    //             this.isShowMsgPoint = true;
                    //             return;
                    //         } else {
                    //             this.isShowMsgPoint = false;
                    //         }
                    //     }
                    // });
                }
            });
        }
        checkPermissions(str) {
            let isHas = false;
            this.permissions.map((value, index) => {
                if (str === value['controller']) {
                    isHas = true;
                }
            });
            return isHas;
        }
        signOut() {
            localStorage.permissions = '';
            localStorage.token = '';
            localStorage.tokenTime = '';
            // window.location.href = window.location.href.split('#')[0] + '#/login';
            window.location.reload();
        }
}
